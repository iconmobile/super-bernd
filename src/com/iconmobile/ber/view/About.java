
package com.iconmobile.ber.view;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.item.AnimatedSpriteMenuItem;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.sprite.Sprite;

import com.iconmobile.ber.manager.ResourcesManager;
import com.iconmobile.ber.util.ID;

public class About extends MenuScene {

    private ResourcesManager resourcesManager;
    public final static int CAMERA_WIDTH = ID.CAMERA_WIDTH;
    public final static int CAMERA_HEIGHT = ID.CAMERA_HEIGHT;
    
    public class AboutMenuType {
        public final static int IMPRINT = 10;
        public final static int LICENCES = 11;
    }

    public About(Camera camera) {
        super(camera);
        resourcesManager = ResourcesManager.getInstance();
        initAbout();
    }

    private void initAbout() {
        setBackgroundEnabled(false);

        Rectangle rect = new Rectangle(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT,
                resourcesManager.getVbom());
        rect.setColor(0, 0, 0);
        rect.setAlpha(0.5f);
        attachChild(rect);
        attachChild(new Sprite(32, 25, resourcesManager.imprint_bg_region, resourcesManager.getVbom()));

        final IMenuItem imprintMenuItem = new AnimatedSpriteMenuItem(AboutMenuType.IMPRINT,
                resourcesManager.about_imprint_region, 
                resourcesManager.getVbom()) {
            @Override
            public void onSelected() {
                super.onSelected();
                this.setCurrentTileIndex(1);
            }

            @Override
            public void onUnselected() {
                super.onUnselected();
                this.setCurrentTileIndex(0);
            }

        };
        imprintMenuItem.setPosition((CAMERA_WIDTH - imprintMenuItem.getWidth()) / 2, 130);
        addMenuItem(imprintMenuItem);
        registerTouchArea(imprintMenuItem);

        final IMenuItem licencesMenuItem = new AnimatedSpriteMenuItem(AboutMenuType.LICENCES,
                resourcesManager.about_licences_region, 
                resourcesManager.getVbom()) {
            @Override
            public void onSelected() {
                super.onSelected();
                this.setCurrentTileIndex(1);
            }

            @Override
            public void onUnselected() {
                super.onUnselected();
                this.setCurrentTileIndex(0);
            }

        };
        licencesMenuItem.setPosition(imprintMenuItem.getX(), imprintMenuItem.getY() + licencesMenuItem.getHeight() + 6);
        addMenuItem(licencesMenuItem);
        registerTouchArea(licencesMenuItem);
    }

}
