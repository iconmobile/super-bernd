package com.iconmobile.ber.manager.object;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.PathModifier;
import org.andengine.entity.modifier.PathModifier.Path;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.iconmobile.ber.manager.ResourcesManager;
import com.iconmobile.ber.util.ID;

public class Suit extends AnimatedSprite {

	private int spriteSpeedMedium = 50;
	private final long[] PLAYER_ANIMATE_MEDIUM = new long[] {
			spriteSpeedMedium, spriteSpeedMedium, spriteSpeedMedium,
			spriteSpeedMedium, spriteSpeedMedium, spriteSpeedMedium,
			spriteSpeedMedium, spriteSpeedMedium };

	private final int lastAnimSprite = 7;

	public Suit(float x, float y, VertexBufferObjectManager vbo) {
		super(x, y, ResourcesManager.getInstance().suit_region, vbo);
	}

	public void animate() {
		setCurrentTileIndex(0);
		animate(PLAYER_ANIMATE_MEDIUM, 0, lastAnimSprite, true);
	}

	public void stop() {
		stopAnimation();
		setCurrentTileIndex(8);
	}
	
	public void good() {
        stopAnimation();
        setCurrentTileIndex(10);
    }
	
	public void bad() {
        stopAnimation();
        setCurrentTileIndex(11);
    }

	public void crash() {
		stopAnimation();
		setCurrentTileIndex(16);
		final Path path = new Path(3).to(getX(), getY())
				.to(getX() - 30, getY() - 20).to(getX() - 50, ID.CAMERA_HEIGHT);
		registerEntityModifier(new ParallelEntityModifier(new PathModifier(
				0.5f, path) {
			@Override
			protected void onModifierFinished(IEntity pItem) {
				super.onModifierFinished(pItem);
				Suit.this.setVisible(false);
			}
		}, new RotationModifier(0.5f, 0, -30)));
	}

}
