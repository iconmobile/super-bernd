
package com.iconmobile.ber.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontUtils;

import android.content.Context;

import com.iconmobile.ber.R;

public class Misc {

    /**
     * Wraps word to width, made by http://www.andengine.org/forums/gles1/word-wrap-function-here-t5680.html
     * 
     * @return normalizedText String
     */
    public static String getNormalizedText(Font font, String ptext, float textWidth) {
        // no need to normalize, its just one word, so return
        if (!ptext.contains(" "))
            return ptext;
        String[] words = ptext.split(" ");
        StringBuilder normalizedText = new StringBuilder();
        StringBuilder line = new StringBuilder();

        for (int i = 0; i < words.length; i++) {
            if (FontUtils.measureText(font, (line + words[i])) > (textWidth)) {
                normalizedText.append(line).append('\n');
                line = new StringBuilder();
            }

            if (line.length() == 0)
                line.append(words[i]);
            else
                line.append(' ').append(words[i]);

            if (i == words.length - 1)
                normalizedText.append(line);
        }
        return normalizedText.toString();
    }

    public static String getStartString(Context context) {
        List<String> text = new ArrayList<String>();
        text.add(context.getString(R.string.start01));
        text.add(context.getString(R.string.start02));
        text.add(context.getString(R.string.start03));
        Collections.shuffle(text);
        return text.get(0);
    }

    public static String getGameOverString(Context context) {
        List<String> text = new ArrayList<String>();
        text.add(context.getString(R.string.gameover01));
        text.add(context.getString(R.string.gameover02));
        text.add(context.getString(R.string.gameover03));
        text.add(context.getString(R.string.gameover04));
        text.add(context.getString(R.string.gameover05));
        Collections.shuffle(text);
        return text.get(0);
    }
    
    public static String getImprintString(Context context) {
        return context.getString(R.string.imprint);
    }

    public static String getEndGoodString(Context context) {
        List<String> text = new ArrayList<String>();
        text.add(context.getString(R.string.end_good01));
        text.add(context.getString(R.string.end_good02));
        text.add(context.getString(R.string.end_good03));
        text.add(context.getString(R.string.end_good04));
        text.add(context.getString(R.string.end_good05));
        Collections.shuffle(text);
        return text.get(0);
    }

    public static String getEndBadString(Context context) {
        List<String> text = new ArrayList<String>();
        text.add(context.getString(R.string.end_bad01));
        text.add(context.getString(R.string.end_bad02));
        text.add(context.getString(R.string.end_bad03));
        text.add(context.getString(R.string.end_bad04));
        text.add(context.getString(R.string.end_bad05));
        Collections.shuffle(text);
        return text.get(0);
    }

}
