package com.iconmobile.ber.manager;

import org.andengine.engine.Engine;
import org.andengine.entity.shape.IShape;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsWorld;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.iconmobile.ber.controller.scene.util.BaseScene;
import com.iconmobile.ber.util.UserData;

public class ActionManager implements ContactListener {

	public enum ContactType {
		BEGIN_HERO_GROUND, END_HERO_GROUND, BEGIN_HERO_FINISH, BEGIN_HERO_BOX, BEGIN_HERO_COIN, BEGIN_HERO_STAGE
	}

	public interface IOnContactListener {
		public void onContact(ContactType type, Body b1, Body b2);
	}

	private PhysicsWorld physicsWorld;
	private Engine engine;
	private BaseScene scene;

	private IOnContactListener callback;

	public ActionManager(BaseScene scene, PhysicsWorld physicsWorld, Engine engine) {
		super();
		this.physicsWorld = physicsWorld;
		this.engine = engine;
		this.scene = scene;
	}

	@Override
	public void beginContact(Contact contact) {
		Body b1 = contact.getFixtureA().getBody();
		Body b2 = contact.getFixtureB().getBody();
		UserData d1 = (UserData) b1.getUserData();
		UserData d2 = (UserData) b2.getUserData();
		if (callback != null) {
			if (isContactBetween(contact, UserData.Type.HERO,
					UserData.Type.GROUND)) {
				callback.onContact(ContactType.BEGIN_HERO_GROUND, contact
						.getFixtureA().getBody(), contact.getFixtureB()
						.getBody());
			}
			if (isContactBetween(contact, UserData.Type.HERO,
					UserData.Type.FINISH)) {
				callback.onContact(ContactType.BEGIN_HERO_FINISH, contact
						.getFixtureA().getBody(), contact.getFixtureB()
						.getBody());
			}
			if (isContactBetween(contact, UserData.Type.HERO, UserData.Type.CONE)) {
				final Body b;
				final IShape s;
				if (d1.getType().equals(UserData.Type.CONE)) {
					b = b1;
					s = d1.getShape();
				} else {
					b = b2;
					s = d2.getShape();
				}
				final PhysicsConnector physicsConnector = physicsWorld
						.getPhysicsConnectorManager()
						.findPhysicsConnectorByShape(d1.getShape());
				engine.runOnUpdateThread(new Runnable() {
					@Override
					public void run() {
						if (physicsConnector != null) {
							physicsWorld
									.unregisterPhysicsConnector(physicsConnector);
							b.setActive(false);
							physicsWorld.destroyBody(b);
							s.setVisible(false);
							s.setIgnoreUpdate(true);
							scene.detachChild(s);
						}
					}
				});
				callback.onContact(ContactType.BEGIN_HERO_BOX, contact
						.getFixtureA().getBody(), contact.getFixtureB()
						.getBody());
			}
			if (isContactBetween(contact, UserData.Type.HERO, UserData.Type.GLASS)) {
                final Body b;
                final IShape s;
                if (d1.getType().equals(UserData.Type.GLASS)) {
                    b = b1;
                    s = d1.getShape();
                } else {
                    b = b2;
                    s = d2.getShape();
                }
                final PhysicsConnector physicsConnector = physicsWorld
                        .getPhysicsConnectorManager()
                        .findPhysicsConnectorByShape(d1.getShape());
                engine.runOnUpdateThread(new Runnable() {
                    @Override
                    public void run() {
                        if (physicsConnector != null) {
                            physicsWorld
                                    .unregisterPhysicsConnector(physicsConnector);
                            b.setActive(false);
                            physicsWorld.destroyBody(b);
                            s.setVisible(false);
                            s.setIgnoreUpdate(true);
                            scene.detachChild(s);
                        }
                    }
                });
                callback.onContact(ContactType.BEGIN_HERO_BOX, contact
                        .getFixtureA().getBody(), contact.getFixtureB()
                        .getBody());
            }
			if (isContactBetween(contact, UserData.Type.HERO,
					UserData.Type.TOOLBOX)) {
				final Body b;
				final IShape s;
				if (d1.getType().equals(UserData.Type.TOOLBOX)) {
					b = b1;
					s = d1.getShape();
				} else {
					b = b2;
					s = d2.getShape();
				}
				final PhysicsConnector physicsConnector = physicsWorld
						.getPhysicsConnectorManager()
						.findPhysicsConnectorByShape(d1.getShape());
				engine.runOnUpdateThread(new Runnable() {
					@Override
					public void run() {
						if (physicsConnector != null) {
							physicsWorld
									.unregisterPhysicsConnector(physicsConnector);
							b.setActive(false);
							physicsWorld.destroyBody(b);
							s.setVisible(false);
							s.setIgnoreUpdate(true);
							scene.detachChild(s);
						}
					}
				});
				callback.onContact(ContactType.BEGIN_HERO_COIN, contact
						.getFixtureA().getBody(), contact.getFixtureB()
						.getBody());
			}
			if (isContactBetween(contact, UserData.Type.HERO,
					UserData.Type.STAGE)) {
				final Body b;
				if (d1.getType().equals(UserData.Type.HERO)) {
					b = b1;
				} else {
					b = b2;
				}
				final PhysicsConnector physicsConnector = physicsWorld
						.getPhysicsConnectorManager()
						.findPhysicsConnectorByShape(d1.getShape());
				engine.runOnUpdateThread(new Runnable() {
					@Override
					public void run() {
						if (physicsConnector != null) {
							physicsWorld
									.unregisterPhysicsConnector(physicsConnector);
							b.setActive(false);
							physicsWorld.destroyBody(b);
						}
					}
				});
				callback.onContact(ContactType.BEGIN_HERO_STAGE, contact
						.getFixtureA().getBody(), contact.getFixtureB()
						.getBody());
			}
		}
	}

	@Override
	public void endContact(Contact contact) {
		if (callback != null) {
			if (isContactBetween(contact, UserData.Type.HERO,
					UserData.Type.GROUND)) {
				callback.onContact(ContactType.END_HERO_GROUND, contact
						.getFixtureA().getBody(), contact.getFixtureB()
						.getBody());
			}
		}
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
	}

	public void setOnContactListener(IOnContactListener callback) {
		this.callback = callback;
	}

	private boolean isContactBetween(Contact contact, UserData.Type idOne,
			UserData.Type idTwo) {
		final Object x1 = contact.getFixtureA().getBody().getUserData();
		final Object x2 = contact.getFixtureB().getBody().getUserData();
		if (x1 instanceof UserData && x2 instanceof UserData) {
			UserData ud1 = (UserData) x1;
			UserData ud2 = (UserData) x2;
			if (ud1.getType().equals(idOne) && ud2.getType().equals(idTwo)
					|| ud1.getType().equals(idTwo)
					&& ud2.getType().equals(idOne)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

}
