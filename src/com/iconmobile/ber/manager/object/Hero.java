package com.iconmobile.ber.manager.object;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.PathModifier;
import org.andengine.entity.modifier.PathModifier.Path;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.iconmobile.ber.manager.ResourcesManager;
import com.iconmobile.ber.util.ID;
import com.iconmobile.ber.util.UserData;

public class Hero extends AnimatedSprite {

	private Body body;

	private boolean footContact;
	private boolean isCrouching;
	private boolean isFlaf;

	private int spriteSpeedMedium = 35;
	private final long[] PLAYER_ANIMATE_MEDIUM = new long[] {
			spriteSpeedMedium, spriteSpeedMedium, spriteSpeedMedium,
			spriteSpeedMedium, spriteSpeedMedium, spriteSpeedMedium,
			spriteSpeedMedium, spriteSpeedMedium, spriteSpeedMedium,
			spriteSpeedMedium, spriteSpeedMedium, spriteSpeedMedium,
			spriteSpeedMedium, spriteSpeedMedium };

	private int spriteSpeedSlow = 50;
	private final long[] PLAYER_ANIMATE_SLOW = new long[] { spriteSpeedSlow,
			spriteSpeedSlow, spriteSpeedSlow, spriteSpeedSlow, spriteSpeedSlow,
			spriteSpeedSlow, spriteSpeedSlow, spriteSpeedSlow, spriteSpeedSlow,
			spriteSpeedSlow, spriteSpeedSlow, spriteSpeedSlow, spriteSpeedSlow,
			spriteSpeedSlow };

	private final int lastAnimSprite = 13;

	public Body getBody() {
		return body;
	}

	public boolean isCrouching() {
		return this.isCrouching;
	}

	public void setFoodContact(boolean footContact) {
		this.footContact = footContact;
	}

	public Hero(float x, float y, VertexBufferObjectManager vbo,
			PhysicsWorld physicsWorld) {
		super(x, y, ResourcesManager.getInstance().hero_region, vbo);
		createPhysics(x, y , vbo, physicsWorld);
		footContact = true;
		isCrouching = false;
		isFlaf = false;
	}

	private void createPhysics(float x, float y, VertexBufferObjectManager vbo, PhysicsWorld physicsWorld) {
	    Rectangle rec = new Rectangle(x, y, ID.HERO_WIDTH - 10, ID.HERO_HEIGHT,
                vbo);
	    
		body = PhysicsFactory.createBoxBody(physicsWorld, rec,
				BodyType.DynamicBody, PhysicsFactory.createFixtureDef(1, 0, 0));
		body.setUserData(new UserData(UserData.Type.HERO, null));
		body.setFixedRotation(true);
		body.setAwake(false);
		body.setBullet(true);

		physicsWorld.registerPhysicsConnector(new PhysicsConnector(this, body,
				true, true));
	}

	public void moveMedium() {
		body.setLinearVelocity(new Vector2(17, body.getLinearVelocity().y));
		setCurrentTileIndex(0);
		animate(PLAYER_ANIMATE_MEDIUM, 0, lastAnimSprite, true);
	}

	public void moveSlow() {
		body.setLinearVelocity(new Vector2(10, body.getLinearVelocity().y));
		setCurrentTileIndex(0);
		animate(PLAYER_ANIMATE_SLOW, 0, lastAnimSprite, true);
	}

	public void stop() {
		body.setLinearVelocity(new Vector2(0, 0));
		stopAnimation();
		setCurrentTileIndex(17);
	}

	public void crash() {
		stopAnimation();
		setCurrentTileIndex(16);
		final Path path = new Path(3).to(getX(), getY())
				.to(getX() - 30, getY() - 20).to(getX() - 50, ID.CAMERA_HEIGHT);
		registerEntityModifier(new ParallelEntityModifier(new PathModifier(
				0.5f, path) {
			@Override
			protected void onModifierFinished(IEntity pItem) {
				super.onModifierFinished(pItem);
				Hero.this.setVisible(false);
			}
		}, new RotationModifier(0.5f, 0, -30)));
	}

	public void run() {
		if (footContact) {
			isCrouching = false;
			setCurrentTileIndex(0);
			animate(PLAYER_ANIMATE_MEDIUM, 0, lastAnimSprite, true);
			stand();
		} else {
		    isCrouching = false;
            setCurrentTileIndex(11);
            stand();
		}
	}

	public boolean jump() {
		if (footContact) {
			isCrouching = false;
			stopAnimation();
			setCurrentTileIndex(15);
			stand();
			body.setLinearVelocity(new Vector2(body.getLinearVelocity().x, -16f));
			return true;
		} else {
			return false;
		}
	}

	public void crouch() {
		isCrouching = true;
		stopAnimation();
		setCurrentTileIndex(14);
		lie();
		body.setLinearVelocity(new Vector2(body.getLinearVelocity().x, 55f));
	}

	public void lie() {
    	if (!isFlaf) {
    		isFlaf = true;
    		final float angle = (float) -Math.toRadians(90);
    		body.setTransform(body.getPosition(), angle);
    	}
    }

	public void stand() {
		if (isFlaf) {
			isFlaf = false;
			body.setTransform(body.getPosition(), 0);
		}
	}

}
