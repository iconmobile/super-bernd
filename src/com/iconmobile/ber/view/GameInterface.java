
package com.iconmobile.ber.view;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.PathModifier;
import org.andengine.entity.modifier.PathModifier.Path;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.color.Color;

import android.view.MotionEvent;

import com.iconmobile.ber.R;
import com.iconmobile.ber.controller.IFacebookController;
import com.iconmobile.ber.manager.ResourcesManager;
import com.iconmobile.ber.manager.object.Suit;
import com.iconmobile.ber.util.ID;
import com.iconmobile.ber.util.Misc;

public class GameInterface extends HUD {

    public enum GameInterfaceType {
        JUMP, CROUCH, PAUSE, DONATE, SHARE, RESTART, START, END, TUTORIAL_END
    }

    public interface IOnGameInterfaceClickListener {
        public void onOnGameInterfaceClicked(GameInterfaceType type,
                TouchEvent touchEvent, float x, float y);
    }

    private Text scoreText;
    private Text scoreShadowText;
    private Rectangle controlCrouch;
    private Rectangle controlJump;
    private TiledSprite pauseButton;

    private Text dialog;

    private Sprite minus;
    private Sprite euro;

    private TiledSprite life1;
    private TiledSprite life2;
    private TiledSprite life3;

    private Sprite bereit;
    private Sprite und;
    private Sprite los;
    private Sprite ok;

    private ResourcesManager resourcesManager;

    private IOnGameInterfaceClickListener callback;
    private IFacebookController fBcontroller;

    public GameInterface(IOnGameInterfaceClickListener callback, IFacebookController fBcontroller) {
        super();
        resourcesManager = ResourcesManager.getInstance();
        this.callback = callback;
        this.fBcontroller = fBcontroller;
        initInterface();
        registerControl();
    }

    private void initInterface() {
        minus = new Sprite(0, 0, resourcesManager.game_score_minus_region, resourcesManager.getVbom());
        euro = new Sprite(0, 0, resourcesManager.game_score_euro_region, resourcesManager.getVbom());
        attachChild(minus);
        attachChild(euro);

        scoreText = new Text(30, 15, resourcesManager.font_8bit, resourcesManager
                .getActivity().getString(R.string.score_pattern),
                resourcesManager.getVbom());
        scoreText.setTextOptions(new TextOptions(HorizontalAlign.RIGHT));
        scoreText.setColor(new Color(251f / 255f, 202f / 255f, 104f / 255f));
        scoreShadowText = new Text(scoreText.getX() - 2, scoreText.getY() + 2,
                resourcesManager.font_8bit, resourcesManager
                        .getActivity().getString(R.string.score_pattern),
                resourcesManager.getVbom());
        scoreShadowText.setTextOptions(new TextOptions(HorizontalAlign.RIGHT));
        scoreShadowText.setColor(new Color(0, 0, 0));
        setScoreText(0);
        attachChild(scoreShadowText);
        attachChild(scoreText);

        bereit = new Sprite(60, 275, resourcesManager.game_start_bereit, resourcesManager.getVbom());
        und = new Sprite(95, 275, resourcesManager.game_start_und, resourcesManager.getVbom());
        los = new Sprite(75, 260, resourcesManager.game_start_los, resourcesManager.getVbom());
        ok = new Sprite(0, 0, resourcesManager.game_start_ok, resourcesManager.getVbom());

        bereit.setVisible(false);
        bereit.registerEntityModifier(new SequenceEntityModifier(
                new ScaleModifier(0.65f, 0.0f, 1.1f),
                new LoopEntityModifier(
                        new SequenceEntityModifier(
                                new ScaleModifier(0.5f, 1.1f, 0.9f),
                                new ScaleModifier(0.5f, 0.9f, 1.1f)))) {
            @Override
            protected void onModifierStarted(IEntity pItem) {
                super.onModifierStarted(pItem);
                bereit.setVisible(true);
            }
        });
        und.setVisible(false);
        und.registerEntityModifier(
                new ScaleModifier(0.5f, 0.9f, 1.1f) {
                    @Override
                    protected void onModifierStarted(IEntity pItem) {
                        super.onModifierStarted(pItem);
                        und.setVisible(true);
                    }
                });
        ok.setVisible(false);
        ok.registerEntityModifier(new SequenceEntityModifier(
                new ScaleModifier(0.65f, 0.0f, 0.8f),
                new LoopEntityModifier(
                        new SequenceEntityModifier(
                                new ScaleModifier(0.5f, 0.8f, 0.6f),
                                new ScaleModifier(0.5f, 0.6f, 0.8f)))) {
            @Override
            protected void onModifierStarted(IEntity pItem) {
                super.onModifierStarted(pItem);
                ok.setVisible(true);
            }
        });

        life1 = new TiledSprite(435, 17,
                resourcesManager.game_score_life_region,
                resourcesManager.getVbom());
        life2 = new TiledSprite(life1.getX() + life1.getWidth() + 10, life1.getY(),
                resourcesManager.game_score_life_region,
                resourcesManager.getVbom());
        life3 = new TiledSprite(life2.getX() + life2.getWidth() + 10, life2.getY(),
                resourcesManager.game_score_life_region,
                resourcesManager.getVbom());
        hideLife();
        attachChild(life1);
        attachChild(life2);
        attachChild(life3);

        setTouchAreaBindingOnActionDownEnabled(true);
        setOnAreaTouchTraversalFrontToBack();

        controlCrouch = new Rectangle(0, ID.CAMERA_HEIGHT / 2, ID.CAMERA_WIDTH,
                ID.CAMERA_HEIGHT / 2, resourcesManager.getVbom()) {
            public boolean onAreaTouched(TouchEvent touchEvent, float x, float y) {
                callback.onOnGameInterfaceClicked(GameInterfaceType.CROUCH,
                        touchEvent, x, y);
                return true;
            };
        };
        controlCrouch.setAlpha(0);
        attachChild(controlCrouch);

        controlJump = new Rectangle(0, 0, ID.CAMERA_WIDTH,
                ID.CAMERA_HEIGHT / 2, resourcesManager.getVbom()) {
            public boolean onAreaTouched(TouchEvent touchEvent, float x, float y) {
                callback.onOnGameInterfaceClicked(GameInterfaceType.JUMP,
                        touchEvent, x, y);
                return true;
            };
        };
        controlJump.setAlpha(0);
        attachChild(controlJump);

        pauseButton = new TiledSprite(700, 15, resourcesManager.game_menu_pause_region, resourcesManager.getVbom()) {
            public boolean onAreaTouched(TouchEvent touchEvent, float x, float y) {
                switch (touchEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_MOVE:
                        this.setCurrentTileIndex(1);
                        break;
                    case MotionEvent.ACTION_UP:
                        this.setCurrentTileIndex(0);
                        callback.onOnGameInterfaceClicked(GameInterfaceType.PAUSE,
                                touchEvent, x, y);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        this.setCurrentTileIndex(0);
                        break;
                }
                return true;
            };
        };
        attachChild(pauseButton);
    }

    public void registerControl() {
        if (callback != null) {
            pauseButton.setVisible(true);
            registerTouchArea(controlCrouch);
            registerTouchArea(controlJump);
            registerTouchArea(pauseButton);
        }
    }

    public void unregisterControl() {
        pauseButton.setVisible(false);
        unregisterTouchArea(controlCrouch);
        unregisterTouchArea(controlJump);
        unregisterTouchArea(pauseButton);
    }

    public void showLife(int lifes) {
        life1.setVisible(true);
        life2.setVisible(true);
        life3.setVisible(true);
        life1.setCurrentTileIndex(1);
        life2.setCurrentTileIndex(1);
        life3.setCurrentTileIndex(1);
        switch (lifes) {
            case 3:
                life3.setCurrentTileIndex(0);
            case 2:
                life2.setCurrentTileIndex(0);
            case 1:
                life1.setCurrentTileIndex(0);
                break;
            default:
                break;
        }
    }

    public void hideLife() {
        life1.setVisible(false);
        life2.setVisible(false);
        life3.setVisible(false);
    }

    public void setScoreText(int score) {
        String mScore = String.valueOf(Math.abs(score));
        mScore = mScore.replace('0', 'O');
        scoreShadowText.setText(resourcesManager.getActivity().getString(R.string.score)
                + "  " + mScore);
        scoreText.setText(resourcesManager.getActivity().getString(R.string.score)
                + "  " + mScore);

        if (score < 0) {
            minus.setVisible(true);
        } else {
            minus.setVisible(false);
        }
        minus.setPosition(scoreShadowText.getX() + 228, 20);
        euro.setVisible(true);
        euro.setPosition(scoreShadowText.getX() + scoreShadowText.getWidth() + 10, 20);
    }

    public void hideScoreText() {
        scoreShadowText.setText("");
        scoreText.setText("");
        euro.setVisible(false);
    }

    public void go() {
        registerUpdateHandler(new IUpdateHandler() {
            @Override
            public void onUpdate(float pSecondsElapsed) {
                unregisterUpdateHandler(this);
                detachChild(und);
            }

            @Override
            public void reset() {
            }
        });
        attachChild(los);
        final TimerHandler removeText = new TimerHandler(0.3f,
                new ITimerCallback() {
                    public void onTimePassed(final TimerHandler pTimerHandler) {
                        unregisterUpdateHandler(pTimerHandler);
                        GameInterface.this.detachChild(los);
                    }
                });
        registerUpdateHandler(removeText);
    }

    public void gameover() {
        final Rectangle rect = new Rectangle(0, 0, ID.CAMERA_WIDTH, ID.CAMERA_HEIGHT,
                resourcesManager.getVbom());
        rect.setColor(0, 0, 0);
        rect.setAlpha(0.5f);
        final Sprite bg = new Sprite(32, 25,
                resourcesManager.game_story_gameover_bg_region,
                resourcesManager.getVbom()) {
            @Override
            public boolean onAreaTouched(TouchEvent touchEvent, float x, float y) {
                callback.onOnGameInterfaceClicked(GameInterfaceType.RESTART, touchEvent, x, y);
                return true;
            }
        };
        final String dialogString = Misc.getNormalizedText(resourcesManager.font_munro,
                Misc.getGameOverString(resourcesManager.getActivity()),
                570);
        dialog = new Text(110, 85, resourcesManager.font_munro,
                dialogString,
                resourcesManager.getVbom());
        dialog.setText("");
        dialog.setColor(0, 0, 0);
        final AnimatedSprite donate = new AnimatedSprite(99, 263,
                resourcesManager.game_story_btn_donate_region,
                resourcesManager.getVbom()) {
            @Override
            public boolean onAreaTouched(TouchEvent touchEvent, float x, float y) {
                switch (touchEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_MOVE:
                        this.setCurrentTileIndex(1);
                        break;
                    case MotionEvent.ACTION_UP:
                        this.setCurrentTileIndex(0);
                        callback.onOnGameInterfaceClicked(GameInterfaceType.DONATE,
                                touchEvent, x, y);
                        final Sprite cons = new Sprite(60, 230,
                                resourcesManager.text_construction,
                                resourcesManager.getVbom());
                        GameInterface.this.attachChild(cons);
                        GameInterface.this.registerUpdateHandler(new TimerHandler(0.2f,
                                new ITimerCallback() {
                                    private int counter = 0;

                                    public void onTimePassed(final TimerHandler pTimerHandler) {
                                        counter++;
                                        cons.setVisible(!cons.isVisible());
                                        if (counter == 3) {
                                            GameInterface.this.unregisterUpdateHandler(pTimerHandler);
                                        } else {
                                            pTimerHandler.reset();
                                        }
                                    }
                                }));
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        this.setCurrentTileIndex(0);
                        break;
                }
                return true;
            }
        };
        final AnimatedSprite share = new AnimatedSprite(donate.getX() + donate.getWidth() + 15, donate.getY(),
                resourcesManager.game_story_btn_share_region,
                resourcesManager.getVbom()) {
            @Override
            public boolean onAreaTouched(TouchEvent touchEvent, float x, float y) {
                switch (touchEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_MOVE:
                        this.setCurrentTileIndex(1);
                        break;
                    case MotionEvent.ACTION_UP:
                        this.setCurrentTileIndex(0);
                        callback.onOnGameInterfaceClicked(GameInterfaceType.SHARE,
                                touchEvent, x, y);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        this.setCurrentTileIndex(0);
                        break;
                }
                return true;
            }
        };

        attachChild(rect);
        attachChild(bg);
        ok.setPosition(627, 197);
        attachChild(ok);
        registerTouchArea(bg);
        attachChild(donate);
        attachChild(dialog);
        registerUpdateHandler(new TimerHandler(0.01f,
                new ITimerCallback() {
                    private int size = dialogString.length();
                    private int counter = 0;

                    public void onTimePassed(final TimerHandler pTimerHandler) {
                        dialog.setText(dialogString.substring(0, ++counter));
                        if (counter == size) {
                            GameInterface.this.unregisterUpdateHandler(pTimerHandler);
                        } else {
                            pTimerHandler.reset();
                        }
                    }
                }));
        registerTouchArea(donate);
        if (fBcontroller.isValidSession()) {
            attachChild(share);
            registerTouchArea(share);
        }
    }

    public void intro() {
        final Suit suit = new Suit(0, 0, resourcesManager.getVbom());
        suit.setPosition(ID.CAMERA_WIDTH, ID.CAMERA_HEIGHT - ID.GROUND_HEIGHT - suit.getHeight());
        attachChild(suit);
        suit.animate();
        final Sprite bubble = new Sprite(53, 75,
                resourcesManager.game_story_bubble_region,
                resourcesManager.getVbom());
        final String dialogString = Misc.getNormalizedText(resourcesManager.font_munro,
                Misc.getStartString(resourcesManager.getActivity()),
                bubble.getWidth() - 30);
        dialog = new Text(bubble.getX() + 15, bubble.getY() + 7, resourcesManager.font_munro,
                dialogString,
                resourcesManager.getVbom());
        dialog.setText("");
        dialog.setColor(0, 0, 0);
        final Rectangle rect = new Rectangle(0, 0, ID.CAMERA_WIDTH, ID.CAMERA_HEIGHT,
                resourcesManager.getVbom()) {
            @Override
            public boolean onAreaTouched(final TouchEvent touchEvent, final float x, final float y) {
                if (touchEvent.isActionUp()) {
                    GameInterface.this.detachChild(this);
                    GameInterface.this.unregisterTouchArea(this);
                    GameInterface.this.detachChild(bereit);
                    GameInterface.this.attachChild(und);
                    Path pathOut = new Path(2).to(suit.getX(), suit.getY())
                            .to(0 - suit.getWidth(), suit.getY());
                    suit.animate();
                    GameInterface.this.detachChild(bubble);
                    GameInterface.this.detachChild(dialog);
                    suit.registerEntityModifier(new PathModifier(
                            0.8f, pathOut) {
                        @Override
                        protected void onModifierFinished(IEntity pItem) {
                            super.onModifierFinished(pItem);
                            GameInterface.this.detachChild(suit);
                            callback.onOnGameInterfaceClicked(GameInterfaceType.START, touchEvent, x, y);
                        }
                    });
                    return true;
                } else {
                    return false;
                }
            }
        };
        Path pathIn = new Path(2).to(suit.getX(), suit.getY())
                .to(400, suit.getY());
        suit.registerEntityModifier(new PathModifier(
                1.5f, pathIn) {
            @Override
            protected void onModifierFinished(IEntity pItem) {
                super.onModifierFinished(pItem);
                GameInterface.this.attachChild(rect);
                GameInterface.this.registerTouchArea(rect);
                GameInterface.this.attachChild(bubble);
                GameInterface.this.attachChild(dialog);
                GameInterface.this.registerUpdateHandler(new TimerHandler(0.01f,
                        new ITimerCallback() {
                            private int size = dialogString.length();
                            private int counter = 0;

                            public void onTimePassed(final TimerHandler pTimerHandler) {
                                dialog.setText(dialogString.substring(0, ++counter));
                                if (counter == size) {
                                    GameInterface.this.unregisterUpdateHandler(pTimerHandler);
                                } else {
                                    pTimerHandler.reset();
                                }
                            }
                        }));
                suit.stop();
                GameInterface.this.attachChild(bereit);
            }
        });
        rect.setColor(0, 0, 0);
        rect.setAlpha(0.0f);
    }

    public void skipIntro() {
        final Rectangle rect = new Rectangle(0, 0, ID.CAMERA_WIDTH, ID.CAMERA_HEIGHT,
                resourcesManager.getVbom()) {
            @Override
            public boolean onAreaTouched(final TouchEvent touchEvent, final float x, final float y) {
                if (touchEvent.isActionUp()) {
                    GameInterface.this.detachChild(this);
                    GameInterface.this.unregisterTouchArea(this);
                    GameInterface.this.detachChild(bereit);
                    GameInterface.this.attachChild(und);
                    GameInterface.this.registerUpdateHandler(new TimerHandler(0.2f,
                            new ITimerCallback() {
                                public void onTimePassed(final TimerHandler pTimerHandler) {
                                    GameInterface.this.unregisterUpdateHandler(pTimerHandler);
                                    callback.onOnGameInterfaceClicked(GameInterfaceType.START, touchEvent, x, y);
                                }
                            }));
                    return true;
                } else {
                    return false;
                }
            }
        };
        rect.setColor(0, 0, 0);
        rect.setAlpha(0.0f);
        attachChild(rect);
        registerTouchArea(rect);
        attachChild(bereit);
    }

    public void outro(final boolean good) {
        final Suit suit = new Suit(0, 0, resourcesManager.getVbom());
        suit.setPosition(ID.CAMERA_WIDTH, ID.CAMERA_HEIGHT - ID.GROUND_HEIGHT - suit.getHeight());
        attachChild(suit);
        suit.animate();
        final Sprite bubble = new Sprite(53, 100,
                resourcesManager.game_story_bubble_region,
                resourcesManager.getVbom());
        final String dialogString;
        if (good) {
            dialogString = Misc.getNormalizedText(resourcesManager.font_munro,
                    Misc.getEndGoodString(resourcesManager.getActivity()),
                    bubble.getWidth() - 30);
        } else {
            dialogString = Misc.getNormalizedText(resourcesManager.font_munro,
                    Misc.getEndBadString(resourcesManager.getActivity()),
                    bubble.getWidth() - 30);
        }
        dialog = new Text(bubble.getX() + 15, bubble.getY() + 7, resourcesManager.font_munro,
                dialogString,
                resourcesManager.getVbom());
        dialog.setText("");
        dialog.setColor(0, 0, 0);
        final AnimatedSprite donate = new AnimatedSprite(bubble.getX(), 275,
                resourcesManager.game_story_btn_donate_region,
                resourcesManager.getVbom()) {
            @Override
            public boolean onAreaTouched(TouchEvent touchEvent, float x, float y) {
                switch (touchEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_MOVE:
                        this.setCurrentTileIndex(1);
                        break;
                    case MotionEvent.ACTION_UP:
                        this.setCurrentTileIndex(0);
                        callback.onOnGameInterfaceClicked(GameInterfaceType.DONATE,
                                touchEvent, x, y);
                        final Sprite cons = new Sprite(10, 242,
                                resourcesManager.text_construction,
                                resourcesManager.getVbom());
                        GameInterface.this.attachChild(cons);
                        GameInterface.this.registerUpdateHandler(new TimerHandler(0.2f,
                                new ITimerCallback() {
                                    private int counter = 0;

                                    public void onTimePassed(final TimerHandler pTimerHandler) {
                                        counter++;
                                        cons.setVisible(!cons.isVisible());
                                        if (counter == 3) {
                                            GameInterface.this.detachChild(cons);
                                            GameInterface.this.unregisterUpdateHandler(pTimerHandler);
                                        } else {
                                            pTimerHandler.reset();
                                        }
                                    }
                                }));
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        this.setCurrentTileIndex(0);
                        break;
                }
                return true;
            }
        };
        final AnimatedSprite share = new AnimatedSprite(bubble.getX() + bubble.getWidth() - resourcesManager.game_story_btn_share_region.getWidth(), donate.getY(),
                resourcesManager.game_story_btn_share_region,
                resourcesManager.getVbom()) {
            @Override
            public boolean onAreaTouched(TouchEvent touchEvent, float x, float y) {
                switch (touchEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_MOVE:
                        this.setCurrentTileIndex(1);
                        break;
                    case MotionEvent.ACTION_UP:
                        this.setCurrentTileIndex(0);
                        callback.onOnGameInterfaceClicked(GameInterfaceType.SHARE,
                                touchEvent, x, y);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        this.setCurrentTileIndex(0);
                        break;
                }
                return true;
            }
        };
        final Rectangle rect = new Rectangle(0, 0, ID.CAMERA_WIDTH, ID.CAMERA_HEIGHT,
                resourcesManager.getVbom()) {
            @Override
            public boolean onAreaTouched(final TouchEvent touchEvent, final float x, final float y) {
                if (touchEvent.isActionUp()) {
                    GameInterface.this.detachChild(this);
                    GameInterface.this.unregisterTouchArea(this);
                    GameInterface.this.detachChild(donate);
                    GameInterface.this.unregisterTouchArea(donate);
                    if (fBcontroller.isValidSession()) {
                        GameInterface.this.detachChild(share);
                        GameInterface.this.unregisterTouchArea(share);
                    }
                    GameInterface.this.detachChild(ok);
                    Path pathOut = new Path(2).to(suit.getX(), suit.getY())
                            .to(0 - suit.getWidth(), suit.getY());
                    suit.animate();
                    GameInterface.this.detachChild(bubble);
                    GameInterface.this.detachChild(dialog);
                    suit.registerEntityModifier(new PathModifier(
                            0.8f, pathOut) {
                        @Override
                        protected void onModifierFinished(IEntity pItem) {
                            super.onModifierFinished(pItem);
                            GameInterface.this.detachChild(suit);
                            if (good) {
                                callback.onOnGameInterfaceClicked(GameInterfaceType.END, touchEvent, x, y);
                            } else {
                                callback.onOnGameInterfaceClicked(GameInterfaceType.RESTART, touchEvent, x, y);
                            }
                        }
                    });
                    return true;
                } else {
                    return false;
                }
            }
        };
        Path pathIn = new Path(2).to(suit.getX(), suit.getY())
                .to(400, suit.getY());
        suit.registerEntityModifier(new PathModifier(
                1.5f, pathIn) {
            @Override
            protected void onModifierFinished(IEntity pItem) {
                super.onModifierFinished(pItem);
                GameInterface.this.attachChild(rect);
                GameInterface.this.registerTouchArea(rect);
                GameInterface.this.attachChild(bubble);
                GameInterface.this.attachChild(dialog);
                GameInterface.this.registerUpdateHandler(new TimerHandler(0.01f,
                        new ITimerCallback() {
                            private int size = dialogString.length();
                            private int counter = 0;

                            public void onTimePassed(final TimerHandler pTimerHandler) {
                                dialog.setText(dialogString.substring(0, ++counter));
                                if (counter == size) {
                                    GameInterface.this.unregisterUpdateHandler(pTimerHandler);
                                } else {
                                    pTimerHandler.reset();
                                }
                            }
                        }));
                if (good) {
                    suit.good();
                } else {
                    suit.bad();
                }
                ok.setPosition(673, 210);
                attachChild(ok);
                GameInterface.this.attachChild(donate);
                GameInterface.this.registerTouchArea(donate);
                if (fBcontroller.isValidSession()) {
                    GameInterface.this.attachChild(share);
                    GameInterface.this.registerTouchArea(share);
                }
            }
        });
        rect.setColor(0, 0, 0);
        rect.setAlpha(0.0f);
    }

    public void tutorial() {
        final Text jumpText = new Text(80, 108, resourcesManager.font_8bit,
                resourcesManager.getActivity().getString(R.string.tut_jump),
                resourcesManager.getVbom());
        jumpText.setColor(0.f, 0.f, 0.f);
        final Text jumpTextLayer = new Text(-2, -2,
                resourcesManager.font_8bit,
                resourcesManager.getActivity().getString(R.string.tut_jump),
                resourcesManager.getVbom());
        jumpTextLayer.setColor(244 / 255.f, 150 / 255.f, 57 / 255.f);
        jumpText.attachChild(jumpTextLayer);
        final Text slideText = new Text(100, 348,
                resourcesManager.font_8bit,
                resourcesManager.getActivity().getString(R.string.tut_slide),
                resourcesManager.getVbom());
        slideText.setColor(0.f, 0.f, 0.f);
        final Text slideTextLayer = new Text(-2, -2,
                resourcesManager.font_8bit,
                resourcesManager.getActivity().getString(R.string.tut_slide),
                resourcesManager.getVbom());
        slideTextLayer.setColor(244 / 255.f, 150 / 255.f, 57 / 255.f);
        slideText.attachChild(slideTextLayer);
        final Text badItem = new Text(272, 124, resourcesManager.font_munro,
                Misc.getNormalizedText(resourcesManager.font_munro,
                        resourcesManager.getActivity().getString(R.string.tuto_bad_item),
                        385),
                resourcesManager.getVbom());
        final Text goodItem = new Text(badItem.getX(), 263,
                resourcesManager.font_munro,
                Misc.getNormalizedText(resourcesManager.font_munro,
                        resourcesManager.getActivity().getString(R.string.tuto_good_item),
                        385),
                resourcesManager.getVbom());
        final Sprite tutorial = new Sprite(32, 25, resourcesManager.game_tutorial_items, resourcesManager.getVbom()) {
            @Override
            public boolean onAreaTouched(TouchEvent touchEvent, float x, float y) {
                if (touchEvent.isActionUp()) {
                    GameInterface.this.unregisterTouchArea(this);
                    GameInterface.this.detachChild(this);
                    GameInterface.this.detachChild(ok);
                    GameInterface.this.detachChild(badItem);
                    GameInterface.this.detachChild(goodItem);
                    callback.onOnGameInterfaceClicked(GameInterfaceType.TUTORIAL_END, touchEvent, x, y);
                }
                return true;
            }
        };
        final Sprite slide = new Sprite(45, 247, resourcesManager.game_tutorial_area, resourcesManager.getVbom()) {
            @Override
            public boolean onAreaTouched(TouchEvent touchEvent, float x, float y) {
                if (touchEvent.isActionUp()) {
                    GameInterface.this.unregisterTouchArea(this);
                    GameInterface.this.detachChild(this);
                    GameInterface.this.detachChild(slideText);
                    GameInterface.this.attachChild(tutorial);
                    ok.setPosition(610, 350);
                    GameInterface.this.attachChild(ok);
                    GameInterface.this.attachChild(badItem);
                    GameInterface.this.attachChild(goodItem);
                    GameInterface.this.registerTouchArea(tutorial);
                }
                return true;
            }
        };
        final Sprite jump = new Sprite(45, 7, resourcesManager.game_tutorial_area, resourcesManager.getVbom()) {
            @Override
            public boolean onAreaTouched(TouchEvent touchEvent, float x, float y) {
                if (touchEvent.isActionUp()) {
                    GameInterface.this.unregisterTouchArea(this);
                    GameInterface.this.detachChild(this);
                    GameInterface.this.detachChild(jumpText);
                    GameInterface.this.attachChild(slide);
                    GameInterface.this.attachChild(slideText);
                    GameInterface.this.registerTouchArea(slide);
                }
                return true;
            }
        };
        jump.setVisible(false);
        jump.registerEntityModifier(new SequenceEntityModifier(
                new ScaleModifier(0.65f, 0.0f, 1.05f),
                new LoopEntityModifier(
                        new SequenceEntityModifier(
                                new ScaleModifier(0.5f, 1.05f, 0.95f),
                                new ScaleModifier(0.5f, 0.95f, 1.05f)))) {
            @Override
            protected void onModifierStarted(IEntity pItem) {
                super.onModifierStarted(pItem);
                jump.setVisible(true);
            }
        });
        jumpText.setVisible(false);
        jumpText.registerEntityModifier(new SequenceEntityModifier(
                new ScaleModifier(0.65f, 0.0f, 1.02f),
                new LoopEntityModifier(
                        new SequenceEntityModifier(
                                new ScaleModifier(0.5f, 1.02f, 0.98f),
                                new ScaleModifier(0.5f, 0.98f, 1.02f)))) {
            @Override
            protected void onModifierStarted(IEntity pItem) {
                super.onModifierStarted(pItem);
                jumpText.setVisible(true);
            }
        });
        slide.setVisible(false);
        slide.registerEntityModifier(new SequenceEntityModifier(
                new ScaleModifier(0.65f, 0.0f, 1.02f),
                new LoopEntityModifier(
                        new SequenceEntityModifier(
                                new ScaleModifier(0.5f, 1.02f, 0.98f),
                                new ScaleModifier(0.5f, 0.98f, 1.02f)))) {
            @Override
            protected void onModifierStarted(IEntity pItem) {
                super.onModifierStarted(pItem);
                slide.setVisible(true);
            }
        });
        slideText.setVisible(false);
        slideText.registerEntityModifier(new SequenceEntityModifier(
                new ScaleModifier(0.65f, 0.0f, 1.05f),
                new LoopEntityModifier(
                        new SequenceEntityModifier(
                                new ScaleModifier(0.5f, 1.05f, 0.95f),
                                new ScaleModifier(0.5f, 0.95f, 1.05f)))) {
            @Override
            protected void onModifierStarted(IEntity pItem) {
                super.onModifierStarted(pItem);
                slideText.setVisible(true);
            }
        });
        attachChild(jump);
        attachChild(jumpText);
        registerTouchArea(jump);
    }

}
