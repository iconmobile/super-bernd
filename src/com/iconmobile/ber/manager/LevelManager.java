
package com.iconmobile.ber.manager;

import org.andengine.engine.camera.SmoothCamera;
import org.andengine.entity.IEntity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.debugdraw.DebugRenderer;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.util.SAXUtils;
import org.andengine.util.level.IEntityLoader;
import org.andengine.util.level.LevelLoader;
import org.andengine.util.level.constants.LevelConstants;
import org.xml.sax.Attributes;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.iconmobile.ber.controller.scene.util.BaseScene;
import com.iconmobile.ber.util.ID;
import com.iconmobile.ber.util.UserData;
import com.iconmobile.ber.view.util.ParallaxLayer;
import com.iconmobile.ber.view.util.ParallaxLayer.ParallaxEntity;

public class LevelManager extends LevelLoader {

    private static final String TAG_ENTITY = "entity";
    private static final String TAG_ENTITY_ATTRIBUTE_X = "x";
    private static final String TAG_ENTITY_ATTRIBUTE_Y = "y";
    private static final String TAG_ENTITY_ATTRIBUTE_TYPE = "type";
    private static final String TAG_ENTITY_ATTRIBUTE_WIDTH = "width";
    private static final String TAG_ENTITY_ATTRIBUTE_HEIGHT = "height";

    private static final Object TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_CONE = "cone";
    private static final Object TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_GLASS = "glass";
    private static final Object TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_PALLET = "pallet";
    private static final Object TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_TOOLBOX = "toolbox";
    private static final Object TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_STAGE = "stage";
    private static final Object TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_FENCE = "fence";

    private BaseScene scene;
    private PhysicsWorld physicsWorld;
    private SmoothCamera camera;
    private ResourcesManager resourcesManager;

    private int perfectScore;
    private int levelWidth;

    public LevelManager(BaseScene scene, PhysicsWorld physicsWorld, SmoothCamera camera) {
        super();
        this.perfectScore = 0;
        this.scene = scene;
        this.physicsWorld = physicsWorld;
        this.camera = camera;
        this.resourcesManager = ResourcesManager.getInstance();
        initLevel();
    }

    public int getPerfectScore() {
        return this.perfectScore;
    }

    public int getLevelWidth() {
        return levelWidth;
    }

    public void initLevel() {
        registerEntityLoader(LevelConstants.TAG_LEVEL, new IEntityLoader() {
            @Override
            public IEntity onLoadEntity(final String pEntityName,
                    final Attributes pAttributes) {
                final int width = SAXUtils.getIntAttributeOrThrow(pAttributes,
                        LevelConstants.TAG_LEVEL_ATTRIBUTE_WIDTH);

                levelWidth = width;

                createBackground(width);
                createGround(width);

                if (ID.DEBUG) {
                    DebugRenderer debug = new DebugRenderer(physicsWorld,
                            resourcesManager.getVbom());
                    scene.attachChild(debug);
                }

                return scene;
            }
        });

        registerEntityLoader(TAG_ENTITY, new IEntityLoader() {
            @Override
            public IEntity onLoadEntity(final String pEntityName,
                    final Attributes pAttributes) {
                final int x = SAXUtils.getIntAttributeOrThrow(pAttributes,
                        TAG_ENTITY_ATTRIBUTE_X);
                final int y = SAXUtils.getIntAttributeOrThrow(pAttributes,
                        TAG_ENTITY_ATTRIBUTE_Y);
                final String type = SAXUtils.getAttributeOrThrow(pAttributes,
                        TAG_ENTITY_ATTRIBUTE_TYPE);
                final int width = SAXUtils.getIntAttributeOrThrow(pAttributes,
                        TAG_ENTITY_ATTRIBUTE_WIDTH);
                final int height = SAXUtils.getIntAttributeOrThrow(pAttributes,
                        TAG_ENTITY_ATTRIBUTE_HEIGHT);

                final Sprite levelObject;
                final FixtureDef FIXTURE_DEF = PhysicsFactory.createFixtureDef(
                        1, 0, 0);

                if (type.equals(TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_CONE)) {
                    levelObject = new Sprite(x, y, width, height,
                            resourcesManager.game_obj_cone_region, resourcesManager
                                    .getVbom());
                    FIXTURE_DEF.isSensor = true;
                    Body body = PhysicsFactory.createBoxBody(physicsWorld,
                            levelObject, BodyType.StaticBody, FIXTURE_DEF);
                    body.setUserData(new UserData(UserData.Type.CONE, levelObject));
                    physicsWorld.registerPhysicsConnector(new PhysicsConnector(
                            levelObject, body, true, false));
                } else if (type.equals(TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_GLASS)) {
                    levelObject = new Sprite(x, y, width, height,
                            resourcesManager.game_obj_glass_region, resourcesManager
                                    .getVbom());
                    FIXTURE_DEF.isSensor = true;
                    Body body = PhysicsFactory.createBoxBody(physicsWorld,
                            levelObject, BodyType.StaticBody, FIXTURE_DEF);
                    body.setUserData(new UserData(UserData.Type.GLASS, levelObject));
                    physicsWorld.registerPhysicsConnector(new PhysicsConnector(
                            levelObject, body, true, false));
                } else if (type.equals(TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_FENCE)) {
                    levelObject = new Sprite(x, y, width, height,
                            resourcesManager.game_obj_fence_region, resourcesManager
                                    .getVbom());
                } else if (type.equals(TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_TOOLBOX)) {
                    levelObject = new Sprite(x, y, width, height,
                            resourcesManager.game_obj_toolbox_region, resourcesManager
                                    .getVbom()) {
                    };
                    FIXTURE_DEF.isSensor = true;
                    Body body = PhysicsFactory.createBoxBody(physicsWorld,
                            levelObject, BodyType.StaticBody, FIXTURE_DEF);
                    body.setUserData(new UserData(UserData.Type.TOOLBOX, levelObject));
                    physicsWorld.registerPhysicsConnector(new PhysicsConnector(
                            levelObject, body, true, false));
                    perfectScore += 10;
                } else if (type.equals(TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_PALLET)) {
                    Rectangle crashOne = new Rectangle(x + 8, y + height - 20, 107, 20,
                            resourcesManager.getVbom());
                    Body crashBodyOne = PhysicsFactory.createBoxBody(physicsWorld,
                            crashOne, BodyType.StaticBody, FIXTURE_DEF);
                    crashBodyOne.setUserData(new UserData(UserData.Type.STAGE, crashOne));
                    physicsWorld.registerPhysicsConnector(new PhysicsConnector(
                            crashOne, crashBodyOne, true, false));
                    crashOne.setIgnoreUpdate(true);
                    crashOne.setCullingEnabled(true);
                    Rectangle crashTwo = new Rectangle(x + 57, y, 6, height - 20,
                            resourcesManager.getVbom());
                    Body crashBodyTwo = PhysicsFactory.createBoxBody(physicsWorld,
                            crashTwo, BodyType.StaticBody, FIXTURE_DEF);
                    crashBodyTwo.setUserData(new UserData(UserData.Type.STAGE, crashOne));
                    physicsWorld.registerPhysicsConnector(new PhysicsConnector(
                            crashOne, crashBodyTwo, true, false));
                    crashTwo.setIgnoreUpdate(true);
                    crashTwo.setCullingEnabled(true);

                    levelObject = new Sprite(x, y, width, height,
                            resourcesManager.game_obj_pallet_region, resourcesManager
                                    .getVbom()) {
                    };
                } else if (type.equals(TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_STAGE)) {
                    int widthFront = 9;
                    int widthBack = 10;
                    Rectangle crash = new Rectangle(x + 3, y + 6, 1, height - 6,
                            resourcesManager.getVbom());
                    FIXTURE_DEF.isSensor = true;
                    Body crashBody = PhysicsFactory.createBoxBody(physicsWorld,
                            crash, BodyType.StaticBody, FIXTURE_DEF);
                    crashBody.setUserData(new UserData(UserData.Type.STAGE, crash));
                    physicsWorld.registerPhysicsConnector(new PhysicsConnector(
                            crash, crashBody, true, false));
                    crash.setIgnoreUpdate(true);
                    crash.setCullingEnabled(true);

                    Sprite front = new Sprite(x, y, widthFront, height,
                            resourcesManager.game_stage_front_region,
                            resourcesManager.getVbom());
                    front.setIgnoreUpdate(true);
                    front.setCullingEnabled(true);
                    scene.attachChild(front);
                    Sprite back = new Sprite(x + width - widthBack, y, widthBack, height,
                            resourcesManager.game_stage_back_region,
                            resourcesManager.getVbom());
                    back.setIgnoreUpdate(true);
                    back.setCullingEnabled(true);
                    scene.attachChild(back);
                    levelObject = new Sprite(x + widthFront, y, width - widthFront - widthBack, height,
                            resourcesManager.game_stage_middle_region,
                            resourcesManager.getVbom());
                    FIXTURE_DEF.isSensor = false;
                    Rectangle stage = new Rectangle(x + 4, y + 5, width - 6, height - 5,
                            resourcesManager.getVbom());
                    stage.setIgnoreUpdate(true);
                    stage.setCullingEnabled(true);
                    Body body = PhysicsFactory.createBoxBody(physicsWorld,
                            stage, BodyType.StaticBody, FIXTURE_DEF);
                    body.setUserData(new UserData(UserData.Type.GROUND, stage));
                    physicsWorld.registerPhysicsConnector(new PhysicsConnector(
                            stage, body, true, false));
                } else {
                    throw new IllegalArgumentException();
                }

                levelObject.setIgnoreUpdate(true);
                levelObject.setCullingEnabled(true);

                return levelObject;
            }
        });
    }

    private void createBackground(int width) {
        final ParallaxLayer parallaxBackground = new ParallaxLayer(camera,
                true, width);
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(-1.0f,
                new Sprite(0, 0, resourcesManager.game_bg_00_region,
                        resourcesManager.getVbom()), true));
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(-0.97f,
                new Sprite(0, 26,
                        resourcesManager.game_bg_01_region,
                        resourcesManager.getVbom()), true));
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(-0.98f,
                new Sprite(0, 254,
                        resourcesManager.game_bg_02_region,
                        resourcesManager.getVbom()), true));
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(-0.975f,
                new Sprite(0, 262,
                        resourcesManager.game_bg_03_region,
                        resourcesManager.getVbom()), true));
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(-0.965f,
                new Sprite(0, 283,
                        resourcesManager.game_bg_04_region,
                        resourcesManager.getVbom()), true));
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(-0.96f,
                new Sprite(0, 292,
                        resourcesManager.game_bg_05_region,
                        resourcesManager.getVbom()), true));
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(-0.955f,
                new Sprite(0, 52,
                        resourcesManager.game_bg_06_region,
                        resourcesManager.getVbom()), true));
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(-0.94f,
                new Sprite(0, 18,
                        resourcesManager.game_bg_07_region,
                        resourcesManager.getVbom()), true));
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(-0.93f,
                new Sprite(00, 296,
                        resourcesManager.game_bg_08_region,
                        resourcesManager.getVbom()), true));
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(-0.85f,
                new Sprite(0, 335,
                        resourcesManager.game_bg_09_region,
                        resourcesManager.getVbom()), true));
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(-0.81f,
                new Sprite(1500, 9,
                        resourcesManager.game_obj_tower_region,
                        resourcesManager.getVbom()), true, 12));
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(-0.81f,
                new Sprite(2100, 314,
                        resourcesManager.game_obj_plane_region,
                        resourcesManager.getVbom()), true, 8));
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(-0.81f,
                new Sprite(1000, 282,
                        resourcesManager.game_obj_dipper_region,
                        resourcesManager.getVbom()), true, 20));
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(-0.8f,
                new Sprite(0, 283,
                        resourcesManager.game_bg_10_region,
                        resourcesManager.getVbom()), true));

        parallaxBackground.attachParallaxEntity(new ParallaxEntity(0f,
                new Sprite(0, ID.CAMERA_HEIGHT - ID.GROUND_HEIGHT,
                        resourcesManager.game_ground_bottom_region, resourcesManager
                                .getVbom()), true));
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(0f,
                new Sprite(0, 410,
                        resourcesManager.game_ground_top_region, resourcesManager
                                .getVbom()), true));
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(0f,
                new Sprite(0, 411,
                        resourcesManager.game_ground_top_region, resourcesManager
                                .getVbom()), true));
        parallaxBackground.setCullingEnabled(true);
        parallaxBackground.setParallaxChangePerSecond(8);
        parallaxBackground.setParallaxScrollFactor(1);

        scene.attachChild(parallaxBackground);
    }

    private void createGround(int width) {
        FixtureDef objectFixtureDef = PhysicsFactory.createFixtureDef(0,
                0, 0);
        Rectangle ground = new Rectangle(-ID.HERO_WIDTH, ID.CAMERA_HEIGHT
                - ID.GROUND_HEIGHT, width + ID.HERO_WIDTH, ID.GROUND_HEIGHT,
                resourcesManager.getVbom());
        ground.setAlpha(0);
        Body ground_body = PhysicsFactory.createBoxBody(physicsWorld, ground,
                BodyType.StaticBody, objectFixtureDef);
        ground_body.setUserData(new UserData(UserData.Type.GROUND, ground));
        ground.setIgnoreUpdate(true);
        ground.setCullingEnabled(true);
        scene.attachChild(ground);

        Rectangle finish = new Rectangle(width, 0, 1, ID.CAMERA_HEIGHT,
                resourcesManager.getVbom());
        finish.setAlpha(0);
        objectFixtureDef.isSensor = true;
        Body body = PhysicsFactory.createBoxBody(physicsWorld, finish,
                BodyType.StaticBody, objectFixtureDef);
        body.setUserData(new UserData(UserData.Type.FINISH, finish));
        physicsWorld.registerPhysicsConnector(new PhysicsConnector(finish,
                body, true, false));
        finish.setCullingEnabled(true);
        finish.setIgnoreUpdate(true);
        scene.attachChild(finish);

        Sprite flagPosts = new Sprite(width - 200, 312,
                resourcesManager.game_obj_flag_posts,
                resourcesManager.getVbom());
        AnimatedSprite flag = new AnimatedSprite(width - 196, 312,
                resourcesManager.game_obj_flag,
                resourcesManager.getVbom());
        flag.animate(150);
        scene.attachChild(flagPosts);
        scene.attachChild(flag);
    }

}
