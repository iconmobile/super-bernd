
package com.iconmobile.ber.manager;

import java.io.IOException;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.SmoothCamera;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;
import org.andengine.util.debug.Debug;

import com.iconmobile.ber.controller.GameActivity;
import com.iconmobile.ber.util.ID;

public class ResourcesManager {

    // Singleton Instance

    private static final ResourcesManager INSTANCE = new ResourcesManager();

    public static ResourcesManager getInstance() {
        return INSTANCE;
    }

    public static void prepareManager(Engine engine, GameActivity activity,
            SmoothCamera camera, VertexBufferObjectManager vbom) {
        getInstance().engine = engine;
        getInstance().activity = activity;
        getInstance().camera = camera;
        getInstance().vbom = vbom;
    }

    // Attributes

    private Engine engine;
    private GameActivity activity;
    private SmoothCamera camera;
    private VertexBufferObjectManager vbom;

    // Splash Assets

    public ITextureRegion splash_region;

    private BuildableBitmapTextureAtlas splashTextureAtlas;

    // Menu Assets

    public ITextureRegion menu_background_region;
    public ITiledTextureRegion menu_play_region;
    public ITiledTextureRegion about_imprint_region;
    public ITiledTextureRegion about_licences_region;
    public ITiledTextureRegion licences_gpl_region;
    public ITiledTextureRegion licences_lgpl_region;

    private BuildableBitmapTextureAtlas menuTextureAtlas;

    public ITiledTextureRegion menu_music_region;
    public ITiledTextureRegion menu_about_region;
    public ITiledTextureRegion menu_facebook_region;
    public ITextureRegion menu_obj_balloon;
    public ITextureRegion menu_obj_plane;

    private BuildableBitmapTextureAtlas menuSubTextureAtlas;

    public ITiledTextureRegion menu_obj_trail;
    public ITextureRegion menu_clouds_back;
    public ITextureRegion menu_clouds_front;
    public ITextureRegion menu_bg_buildings;
    public ITextureRegion text_construction;
    public ITextureRegion imprint_bg_region;

    private BuildableBitmapTextureAtlas menuSecondTextureAtlas;

    public Music musicMenu;

    // Loading Assets

    public ITextureRegion loading_region;

    private BuildableBitmapTextureAtlas loadingTextureAtlas;

    // Game Assets

    public Music musicGame;

    public Font font_munro;
    public Font font_8bit;

    public ITiledTextureRegion game_menu_continue_region;
    public ITiledTextureRegion game_menu_cancel_region;
    public ITiledTextureRegion game_menu_restart_region;
    public ITiledTextureRegion game_menu_pause_region;
    public ITextureRegion game_menu_bg_region;

    private BuildableBitmapTextureAtlas gameMenuTextureAtlas;

    public ITextureRegion game_bg_00_region;
    public ITextureRegion game_bg_01_region;
    public ITextureRegion game_bg_02_region;
    public ITextureRegion game_bg_03_region;
    public ITextureRegion game_bg_04_region;
    public ITextureRegion game_bg_05_region;

    private BuildableBitmapTextureAtlas gameBg01TextureAtlas;

    public ITextureRegion game_bg_06_region;
    public ITextureRegion game_bg_07_region;
    public ITextureRegion game_bg_08_region;
    public ITextureRegion game_bg_09_region;
    public ITextureRegion game_bg_10_region;

    private BuildableBitmapTextureAtlas gameBg02TextureAtlas;

    public ITextureRegion game_ground_top_region;
    public ITextureRegion game_ground_bottom_region;
    public ITextureRegion game_stage_front_region;
    public ITextureRegion game_stage_middle_region;
    public ITextureRegion game_stage_back_region;

    private BuildableBitmapTextureAtlas gameGroundTextureAtlas;

    public ITextureRegion game_obj_cone_region;
    public ITextureRegion game_obj_glass_region;
    public ITextureRegion game_obj_pallet_region;
    public ITextureRegion game_obj_toolbox_region;
    public ITextureRegion game_obj_tower_region;

    private BuildableBitmapTextureAtlas gameObjects01TextureAtlas;

    public ITextureRegion game_obj_fence_region;
    public ITextureRegion game_obj_plane_region;
    public ITextureRegion game_obj_dipper_region;

    public ITextureRegion game_score_minus_region;
    public ITextureRegion game_score_euro_region;
    public ITiledTextureRegion game_score_life_region;

    private BuildableBitmapTextureAtlas gameObjects02TextureAtlas;

    public ITiledTextureRegion hero_region;
    public ITiledTextureRegion suit_region;
    public ITextureRegion game_start_bereit;
    public ITextureRegion game_start_und;
    public ITextureRegion game_start_los;
    public ITextureRegion game_start_ok;

    private BuildableBitmapTextureAtlas gameObjects03TextureAtlas;

    public ITextureRegion game_story_gameover_bg_region;
    public ITextureRegion game_story_bubble_region;
    public ITiledTextureRegion game_story_btn_donate_region;
    public ITiledTextureRegion game_story_btn_share_region;

    private BuildableBitmapTextureAtlas gameStoryTextureAtlas;

    public ITextureRegion game_tutorial_area;
    public ITextureRegion game_tutorial_items;
    public ITiledTextureRegion game_obj_flag;
    public ITextureRegion game_obj_flag_posts;

    private BuildableBitmapTextureAtlas gameTutorialTextureAtlas;

    // Getter / Setter

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public GameActivity getActivity() {
        return activity;
    }

    public void setActivity(GameActivity activity) {
        this.activity = activity;
    }

    public SmoothCamera getCamera() {
        return camera;
    }

    public void setCamera(SmoothCamera camera) {
        this.camera = camera;
    }

    public VertexBufferObjectManager getVbom() {
        return vbom;
    }

    public void setVbom(VertexBufferObjectManager vbom) {
        this.vbom = vbom;
    }

    // Methods

    public void loadMenuResources() {
        loadMenuGraphics();
        loadMenuFonts();
        loadMenuAudio();
    }

    public void loadGameResources() {
        loadGameGraphics();
        loadGameFonts();
        loadGameAudio();
    }

    private void loadMenuGraphics() {
        BitmapTextureAtlasTextureRegionFactory
                .setAssetBasePath(ID.PATH_GRAPHICS + ID.SUBPATH_GRAPHICS_MENU);

        menuTextureAtlas = new BuildableBitmapTextureAtlas(
                activity.getTextureManager(), 1524, 1524, TextureOptions.NEAREST);

        menu_background_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(menuTextureAtlas, activity, ID.GRAPHIC_MENU_BG);
        menu_play_region = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(menuTextureAtlas, activity,
                        ID.GRAPHIC_MENU_BTN_PLAY, 2, 1);

        about_imprint_region = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(menuTextureAtlas, activity,
                        ID.GRAPHIC_ABOUT_IMPRINT_BTN, 2, 1);
        about_licences_region = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(menuTextureAtlas, activity,
                        ID.GRAPHIC_ABOUT_LICENCES_BTN, 2, 1);
        
        licences_gpl_region = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(menuTextureAtlas, activity,
                        ID.GRAPHIC_LICENCES_GPL_BTN, 2, 1);
        licences_lgpl_region = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(menuTextureAtlas, activity,
                        ID.GRAPHIC_LICENCES_LGPL_BTN, 2, 1);
        
        menuSubTextureAtlas = new BuildableBitmapTextureAtlas(
                activity.getTextureManager(), 1024, 1024, TextureOptions.NEAREST);

        menu_music_region = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(menuSubTextureAtlas, activity,
                        ID.GRAPHIC_MENU_BTN_MUSIC, 4, 1);
        menu_facebook_region = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(menuSubTextureAtlas, activity,
                        ID.GRAPHIC_MENU_BTN_FB, 4, 1);
        menu_about_region = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(menuSubTextureAtlas, activity,
                        ID.GRAPHIC_MENU_BTN_ABOUT, 2, 1);

        menu_obj_balloon = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(menuSubTextureAtlas, activity,
                        ID.GRAPHIC_MENU_OBJ_BALLOON);

        menu_obj_plane = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(menuSubTextureAtlas, activity,
                        ID.GRAPHIC_MENU_OBJ_PLANE);

        menuSecondTextureAtlas = new BuildableBitmapTextureAtlas(
                activity.getTextureManager(), 1524, 1524, TextureOptions.NEAREST);

        menu_obj_trail = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(menuSecondTextureAtlas, activity,
                        ID.GRAPHIC_MENU_OBJ_TRAIL, 1, 2);
        menu_clouds_back = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(menuSecondTextureAtlas, activity,
                        ID.GRAPHIC_MENU_CLOUDS_BACK);
        menu_clouds_front = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(menuSecondTextureAtlas, activity,
                        ID.GRAPHIC_MENU_CLOUDS_FRONT);
        menu_bg_buildings = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(menuSecondTextureAtlas, activity,
                        ID.GRAPHIC_MENU_BG_BUILDINGS);
        text_construction = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(menuSecondTextureAtlas, activity,
                        ID.GRAPHIC_MENU_CONSTRUCTION);
        imprint_bg_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(menuSecondTextureAtlas, activity,
                        ID.GRAPHIC_IMPRINT_BG);

        BitmapTextureAtlasTextureRegionFactory
                .setAssetBasePath(ID.PATH_GRAPHICS + ID.SUBPATH_GRAPHICS_LOADING);

        loadingTextureAtlas = new BuildableBitmapTextureAtlas(
                activity.getTextureManager(), 805, 485, TextureOptions.NEAREST);

        loading_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(loadingTextureAtlas, activity,
                        ID.GRAPHIC_LOADING);
        
        

        try {
            this.menuTextureAtlas
                    .build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
                            0, 1, 0));
            this.menuTextureAtlas.load();
            this.menuSubTextureAtlas
                    .build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
                            0, 1, 0));
            this.menuSubTextureAtlas.load();
            this.menuSecondTextureAtlas
                    .build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
                            0, 1, 0));
            this.menuSecondTextureAtlas.load();
            this.loadingTextureAtlas
                    .build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
                            0, 1, 0));
            this.loadingTextureAtlas.load();
        } catch (final TextureAtlasBuilderException e) {
            Debug.e(e);
        }

    }

    private void loadMenuFonts() {
        FontFactory.setAssetBasePath(ID.PATH_FONTS);
        ITexture munroFontTexture = new BitmapTextureAtlas(
                activity.getTextureManager(), 1024, 1024,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        font_munro = FontFactory.createStrokeFromAsset(activity.getFontManager(),
                munroFontTexture, activity.getAssets(), ID.FONT_MUNRO, 27, false,
                Color.WHITE_ABGR_PACKED_INT, 0, 0);
        font_munro.load();
    }

    private void loadMenuAudio() {
        try {
            MusicFactory.setAssetBasePath(ID.PATH_SOUNDS);
            musicMenu = MusicFactory
                    .createMusicFromAsset(getEngine().getMusicManager(),
                            activity,
                            ID.SOUND_MENU);
            musicMenu.setLooping(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadGameGraphics() {
        BitmapTextureAtlasTextureRegionFactory
                .setAssetBasePath(ID.PATH_GRAPHICS + ID.SUBPATH_GRAPHICS_GAME);

        gameMenuTextureAtlas = new BuildableBitmapTextureAtlas(
                activity.getTextureManager(), 1024, 1024,
                TextureOptions.NEAREST);

        game_menu_continue_region = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(gameMenuTextureAtlas, activity,
                        ID.GRAPHIC_GAME_MENU_BTN_CONTINUE, 2, 1);
        game_menu_cancel_region = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(gameMenuTextureAtlas, activity,
                        ID.GRAPHIC_GAME_MENU_BTN_CANCEL, 2, 1);
        game_menu_restart_region = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(gameMenuTextureAtlas, activity,
                        ID.GRAPHIC_GAME_MENU_BTN_RESTART, 2, 1);
        game_menu_bg_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameMenuTextureAtlas, activity,
                        ID.GRAPHIC_GAME_MENU_BG);
        game_menu_pause_region = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(gameMenuTextureAtlas, activity,
                        ID.GRAPHIC_GAME_BTN_PAUSE, 2, 1);

        gameBg01TextureAtlas = new BuildableBitmapTextureAtlas(
                activity.getTextureManager(), 1024, 1024,
                TextureOptions.NEAREST_PREMULTIPLYALPHA);

        game_bg_00_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameBg01TextureAtlas, activity,
                        ID.GRAPHIC_BG_00);
        game_bg_01_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameBg01TextureAtlas, activity,
                        ID.GRAPHIC_BG_01);
        game_bg_02_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameBg01TextureAtlas, activity,
                        ID.GRAPHIC_BG_02);
        game_bg_03_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameBg01TextureAtlas, activity,
                        ID.GRAPHIC_BG_03);
        game_bg_04_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameBg01TextureAtlas, activity,
                        ID.GRAPHIC_BG_04);
        game_bg_05_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameBg01TextureAtlas, activity,
                        ID.GRAPHIC_BG_05);

        gameBg02TextureAtlas = new BuildableBitmapTextureAtlas(
                activity.getTextureManager(), 1024, 1024,
                TextureOptions.NEAREST_PREMULTIPLYALPHA);

        game_bg_06_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameBg02TextureAtlas, activity,
                        ID.GRAPHIC_BG_06);
        game_bg_07_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameBg02TextureAtlas, activity,
                        ID.GRAPHIC_BG_07);
        game_bg_08_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameBg02TextureAtlas, activity,
                        ID.GRAPHIC_BG_08);
        game_bg_09_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameBg02TextureAtlas, activity,
                        ID.GRAPHIC_BG_09);
        game_bg_10_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameBg02TextureAtlas, activity,
                        ID.GRAPHIC_BG_10);

        gameGroundTextureAtlas = new BuildableBitmapTextureAtlas(
                activity.getTextureManager(), 1024, 1024,
                TextureOptions.NEAREST);

        game_ground_top_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameGroundTextureAtlas, activity,
                        ID.GRAPHIC_GROUND_TOP);
        game_ground_bottom_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameGroundTextureAtlas, activity,
                        ID.GRAPHIC_GROUND_BOTTOM);
        game_stage_front_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameGroundTextureAtlas, activity,
                        ID.GRAPHIC_STAGE_FRONT);
        game_stage_middle_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameGroundTextureAtlas, activity,
                        ID.GRAPHIC_STAGE_MIDDLE);
        game_stage_back_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameGroundTextureAtlas, activity,
                        ID.GRAPHIC_STAGE_BACK);

        gameObjects01TextureAtlas = new BuildableBitmapTextureAtlas(
                activity.getTextureManager(), 1024, 1024,
                TextureOptions.NEAREST_PREMULTIPLYALPHA);

        game_obj_cone_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameObjects01TextureAtlas, activity,
                        ID.GRAPHIC_OBJ_CONE);
        game_obj_glass_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameObjects01TextureAtlas, activity,
                        ID.GRAPHIC_OBJ_GLASS);
        game_obj_pallet_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameObjects01TextureAtlas, activity,
                        ID.GRAPHIC_OBJ_PALLET);
        game_obj_toolbox_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameObjects01TextureAtlas, activity,
                        ID.GRAPHIC_OBJ_TOOLBOX);
        game_obj_tower_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameObjects01TextureAtlas, activity,
                        ID.GRAPHIC_OBJ_TOWER);

        gameObjects02TextureAtlas = new BuildableBitmapTextureAtlas(
                activity.getTextureManager(), 1024, 1024,
                TextureOptions.NEAREST_PREMULTIPLYALPHA);

        game_obj_fence_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameObjects02TextureAtlas, activity,
                        ID.GRAPHIC_OBJ_FENCE);
        game_obj_plane_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameObjects02TextureAtlas, activity,
                        ID.GRAPHIC_OBJ_PLANE);
        game_obj_dipper_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameObjects02TextureAtlas, activity,
                        ID.GRAPHIC_OBJ_DIPPER);
        game_score_minus_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameObjects02TextureAtlas, activity,
                        ID.GRAPHIC_SCORE_MINUS);
        game_score_euro_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameObjects02TextureAtlas, activity,
                        ID.GRAPHIC_SCORE_EURO);
        game_score_life_region = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(gameObjects02TextureAtlas, activity,
                        ID.GRAPHIC_SCORE_LIFE, 2, 1);

        gameObjects03TextureAtlas = new BuildableBitmapTextureAtlas(
                activity.getTextureManager(), 1024, 500,
                TextureOptions.NEAREST_PREMULTIPLYALPHA);

        hero_region = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(gameObjects03TextureAtlas, activity,
                        ID.GRAPHIC_HERO, 19, 1);
        suit_region = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(gameObjects03TextureAtlas, activity,
                        ID.GRAPHIC_SUIT, 12, 1);
        game_start_bereit = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameObjects03TextureAtlas, activity,
                        ID.GRAPHIC_START_BEREIT);
        game_start_und = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameObjects03TextureAtlas, activity,
                        ID.GRAPHIC_START_UND);
        game_start_los = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameObjects03TextureAtlas, activity,
                        ID.GRAPHIC_START_LOS);
        game_start_ok = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameObjects03TextureAtlas, activity,
                        ID.GRAPHIC_START_OK);

        gameStoryTextureAtlas = new BuildableBitmapTextureAtlas(
                activity.getTextureManager(), 1024, 1024,
                TextureOptions.NEAREST);

        game_story_btn_donate_region = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(gameStoryTextureAtlas, activity,
                        ID.GRAPHIC_STORY_BTN_DONATE, 2, 1);
        game_story_btn_share_region = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(gameStoryTextureAtlas, activity,
                        ID.GRAPHIC_STORY_BTN_SHARE, 2, 1);
        game_story_gameover_bg_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameStoryTextureAtlas, activity,
                        ID.GRAPHIC_STORY_GAMEOVER_BG);
        game_story_bubble_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameStoryTextureAtlas, activity,
                        ID.GRAPHIC_STORY_BUBBLE);

        gameTutorialTextureAtlas = new BuildableBitmapTextureAtlas(
                activity.getTextureManager(), 1024, 1024,
                TextureOptions.NEAREST);

        game_tutorial_area = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameTutorialTextureAtlas, activity,
                        ID.GRAPHIC_TUTORIAL_AREA);
        game_tutorial_items = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameTutorialTextureAtlas, activity,
                        ID.GRAPHIC_TUTORIAL_ITEMS);
        game_obj_flag_posts = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameTutorialTextureAtlas, activity,
                        ID.GRAPHIC_OBJ_FLAG_POSTS);
        game_obj_flag = BitmapTextureAtlasTextureRegionFactory
                .createTiledFromAsset(gameTutorialTextureAtlas, activity,
                        ID.GRAPHIC_OBJ_FLAG, 1, 2);
        text_construction = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(gameTutorialTextureAtlas, activity,
                        ID.GRAPHIC_MENU_CONSTRUCTION);
        try {
            this.gameMenuTextureAtlas
                    .build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
                            0, 1, 0));
            this.gameMenuTextureAtlas.load();

            this.gameBg01TextureAtlas
                    .build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
                            0, 1, 0));
            this.gameBg01TextureAtlas.load();

            this.gameBg02TextureAtlas
                    .build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
                            0, 1, 0));
            this.gameBg02TextureAtlas.load();

            this.gameGroundTextureAtlas
                    .build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
                            0, 1, 0));
            this.gameGroundTextureAtlas.load();

            this.gameObjects01TextureAtlas
                    .build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
                            0, 1, 0));
            this.gameObjects01TextureAtlas.load();

            this.gameObjects02TextureAtlas
                    .build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
                            0, 1, 0));
            this.gameObjects02TextureAtlas.load();

            this.gameObjects03TextureAtlas
                    .build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
                            0, 1, 0));
            this.gameObjects03TextureAtlas.load();

            this.gameStoryTextureAtlas
                    .build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
                            0, 1, 0));
            this.gameStoryTextureAtlas.load();

            this.gameTutorialTextureAtlas
                    .build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
                            0, 1, 0));
            this.gameTutorialTextureAtlas.load();
        } catch (final TextureAtlasBuilderException e) {
            Debug.e(e);
        }

    }

    private void loadGameFonts() {
        FontFactory.setAssetBasePath(ID.PATH_FONTS);
        ITexture munroFontTexture = new BitmapTextureAtlas(
                activity.getTextureManager(), 1024, 1024,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        font_munro = FontFactory.createStrokeFromAsset(activity.getFontManager(),
                munroFontTexture, activity.getAssets(), ID.FONT_MUNRO, 27, false,
                Color.WHITE_ABGR_PACKED_INT, 0, 0);
        font_munro.load();

        ITexture bitFontTexture = new BitmapTextureAtlas(
                activity.getTextureManager(), 1024, 1024,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        font_8bit = FontFactory.createStrokeFromAsset(activity.getFontManager(),
                bitFontTexture, activity.getAssets(), ID.FONT_8BIT, 30, false,
                Color.WHITE_ABGR_PACKED_INT, 0, 0);
        font_8bit.load();
    }

    private void loadGameAudio() {
        try {
            MusicFactory.setAssetBasePath(ID.PATH_SOUNDS);
            musicGame = MusicFactory
                    .createMusicFromAsset(getEngine().getMusicManager(),
                            activity,
                            ID.SOUND_GAME);
            musicGame.setLooping(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadSplashScreen() {
        BitmapTextureAtlasTextureRegionFactory
                .setAssetBasePath(ID.PATH_GRAPHICS + ID.SUBPATH_GRAPHICS_SPLASH);

        splashTextureAtlas = new BuildableBitmapTextureAtlas(
                activity.getTextureManager(), 805, 485, TextureOptions.BILINEAR);

        splash_region = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(splashTextureAtlas, activity, ID.GRAPHIC_SPLASH);

        try {
            this.splashTextureAtlas
                    .build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
                            0, 1, 0));
            this.splashTextureAtlas.load();
        } catch (final TextureAtlasBuilderException e) {
            Debug.e(e);
        }
    }

    public void unloadSplashScreen() {
        splashTextureAtlas.unload();
        splash_region = null;
    }

    public void unloadMenuTextures() {
        menuTextureAtlas.unload();
        menuSubTextureAtlas.unload();
        menuSecondTextureAtlas.unload();
    }

    public void loadMenuTextures() {
        menuTextureAtlas.load();
        menuSubTextureAtlas.load();
        menuSecondTextureAtlas.load();
    }

    public void unloadGameTextures() {
        gameMenuTextureAtlas.unload();
        gameBg01TextureAtlas.unload();
        gameBg02TextureAtlas.unload();
        gameGroundTextureAtlas.unload();
        gameObjects01TextureAtlas.unload();
        gameObjects02TextureAtlas.unload();
        gameObjects03TextureAtlas.unload();
        gameStoryTextureAtlas.unload();
        gameTutorialTextureAtlas.unload();
    }

    public void loadGameTextures() {
        gameMenuTextureAtlas.load();
        gameBg01TextureAtlas.load();
        gameBg02TextureAtlas.load();
        gameGroundTextureAtlas.load();
        gameObjects01TextureAtlas.load();
        gameObjects02TextureAtlas.load();
        gameObjects03TextureAtlas.load();
        gameStoryTextureAtlas.load();
        gameTutorialTextureAtlas.load();
    }

}
