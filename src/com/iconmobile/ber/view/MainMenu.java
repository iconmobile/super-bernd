
package com.iconmobile.ber.view;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.item.AnimatedSpriteMenuItem;
import org.andengine.entity.scene.menu.item.IMenuItem;

import android.content.Context;

import com.iconmobile.ber.controller.IFacebookController;
import com.iconmobile.ber.manager.ResourcesManager;
import com.iconmobile.ber.manager.SharedPrefsManager;

public class MainMenu extends MenuScene {

    public class MainMenuType {
        public final static int PLAY = 0;
        public final static int MUSIC = 1;
        public final static int FACEBOOK_AUTH = 2;
        public final static int ABOUT = 3;
    }

    private ResourcesManager resourcesManager;
    private IFacebookController callback;
    private Context context;

    public MainMenu(Context context, Camera camera, IFacebookController callback) {
        super(camera);
        this.context = context;
        this.callback = callback;
        this.resourcesManager = ResourcesManager.getInstance();
        initMainMenu();
    }

    public void updateUi() {
        initMainMenu();
    }

    private void initMainMenu() {
        clearMenuItems();

        IMenuItem playMenuItem = new AnimatedSpriteMenuItem(MainMenuType.PLAY,
                resourcesManager.menu_play_region,
                resourcesManager.getVbom()) {
            @Override
            public void onSelected() {
                super.onSelected();
                this.setCurrentTileIndex(1);
            }

            @Override
            public void onUnselected() {
                super.onUnselected();
                this.setCurrentTileIndex(0);
            }
        };
        addMenuItem(playMenuItem);
        playMenuItem.setPosition(231, 100);

        IMenuItem musicMenuItem = new AnimatedSpriteMenuItem(MainMenuType.MUSIC,
                resourcesManager.menu_music_region,
                resourcesManager.getVbom()) {
            @Override
            public void onSelected() {
                super.onSelected();
                if (SharedPrefsManager.getBooleanValue(context,
                        SharedPrefsManager.SHARED_PREF_MUSIC,
                        SharedPrefsManager.SHARED_PREF_MUSIC_DEFAULT)) {
                    this.setCurrentTileIndex(1);
                } else {
                    this.setCurrentTileIndex(3);
                }
            }

            @Override
            public void onUnselected() {
                super.onUnselected();
                if (SharedPrefsManager.getBooleanValue(context,
                        SharedPrefsManager.SHARED_PREF_MUSIC,
                        SharedPrefsManager.SHARED_PREF_MUSIC_DEFAULT)) {
                    this.setCurrentTileIndex(0);
                } else {
                    this.setCurrentTileIndex(2);
                }
            }
        };
        if (SharedPrefsManager.getBooleanValue(context,
                SharedPrefsManager.SHARED_PREF_MUSIC,
                SharedPrefsManager.SHARED_PREF_MUSIC_DEFAULT)) {
            ((AnimatedSpriteMenuItem) musicMenuItem).setCurrentTileIndex(0);
        } else {
            ((AnimatedSpriteMenuItem) musicMenuItem).setCurrentTileIndex(2);
        }
        addMenuItem(musicMenuItem);
        musicMenuItem.setPosition(173, 275);

        IMenuItem facebookMenuItem = new AnimatedSpriteMenuItem(MainMenuType.FACEBOOK_AUTH,
                resourcesManager.menu_facebook_region,
                resourcesManager.getVbom()) {
            @Override
            public void onSelected() {
                super.onSelected();
                if (callback.isValidSession()) {
                    this.setCurrentTileIndex(1);
                } else {
                    this.setCurrentTileIndex(3);
                }
            }

            @Override
            public void onUnselected() {
                super.onUnselected();
                if (callback.isValidSession()) {
                    this.setCurrentTileIndex(0);
                } else {
                    this.setCurrentTileIndex(2);
                }
            }
        };
        if (callback.isValidSession()) {
            ((AnimatedSpriteMenuItem) facebookMenuItem).setCurrentTileIndex(0);
        } else {
            ((AnimatedSpriteMenuItem) facebookMenuItem).setCurrentTileIndex(2);
        }
        addMenuItem(facebookMenuItem);
        facebookMenuItem.setPosition(343, musicMenuItem.getY());

        IMenuItem aboutMenuItem = new AnimatedSpriteMenuItem(MainMenuType.ABOUT,
                resourcesManager.menu_about_region,
                resourcesManager.getVbom()) {
            @Override
            public void onSelected() {
                super.onSelected();
                this.setCurrentTileIndex(1);
            }

            @Override
            public void onUnselected() {
                super.onUnselected();
                this.setCurrentTileIndex(0);
            }
        };
        addMenuItem(aboutMenuItem);
        aboutMenuItem.setPosition(513, musicMenuItem.getY());

        setBackgroundEnabled(false);
    }
}
