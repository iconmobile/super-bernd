package com.iconmobile.ber.manager;

import android.content.Context;
import android.content.SharedPreferences;

public abstract class SharedPrefsManager {
	
	public static final String SHARED_PREF_NAME = "berSharedPrefs";
	
	public static final String SHARED_PREF_MUSIC = "sharedPrefMusic";
	public static final String SHARED_PREF_LIFES = "sharedPrefLives";
	public static final String SHARED_PREF_FIRST_TIME = "sharedPrefFirstTime";
	
	public static final boolean SHARED_PREF_MUSIC_DEFAULT = true;
	public static final int SHARED_PREF_LIFES_DEFAULT = 3;
	public static final boolean SHARED_PREF_FIRST_TIME_DEFAULT = true;

	// GETTER / SETTER
	
	// // Float
      
	public static void setFloatValue(Context context, String pref, float value){
		final SharedPreferences prefs = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
		final SharedPreferences.Editor editor = prefs.edit();
		editor.putFloat(pref, value);
		editor.commit();
	}
	
	public static float getFloatValue(Context context, String pref, float defaultValue){
		final SharedPreferences prefs = context.getSharedPreferences(SharedPrefsManager.SHARED_PREF_NAME, Context.MODE_PRIVATE);
		return prefs.getFloat(pref, defaultValue); 
	}
	
	// // Long
	
	public static void setLongValue(Context context, String pref, long value){
		final SharedPreferences prefs = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
		final SharedPreferences.Editor editor = prefs.edit();
		editor.putLong(pref, value);
		editor.commit();
	}
	
	public static long getLongValue(Context context, String pref, long defaultValue){
		final SharedPreferences prefs = context.getSharedPreferences(SharedPrefsManager.SHARED_PREF_NAME, Context.MODE_PRIVATE);
		return prefs.getLong(pref, defaultValue); 
	}
	
	// // Integer
	
	public static void setIntegerValue(Context context, String pref, int value){
		final SharedPreferences prefs = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
		final SharedPreferences.Editor editor = prefs.edit();
		editor.putInt(pref, value);
		editor.commit();
	}
		
	public static int getIntegerValue(Context context, String pref, int defaultValue){
		final SharedPreferences prefs = context.getSharedPreferences(SharedPrefsManager.SHARED_PREF_NAME, Context.MODE_PRIVATE);
		return prefs.getInt(pref, defaultValue); 
	}
	
	// // Boolean
	
	public static void setBooleanValue(Context context, String pref, boolean value){
		final SharedPreferences prefs = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
		final SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean(pref, value);
		editor.commit();
	}
	
	public static boolean getBooleanValue(Context context, String pref, boolean defaultValue){
		final SharedPreferences prefs = context.getSharedPreferences(SharedPrefsManager.SHARED_PREF_NAME, Context.MODE_PRIVATE);
		return prefs.getBoolean(pref, defaultValue); 
	}
	
	// // String
	
	public static void setStringValue(Context context, String pref, String value){
		final SharedPreferences prefs = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
		final SharedPreferences.Editor editor = prefs.edit();
		editor.putString(pref, value);
		editor.commit();
	}
	
	public static String getStringValue(Context context, String pref, String defaultValue){
		final SharedPreferences prefs = context.getSharedPreferences(SharedPrefsManager.SHARED_PREF_NAME, Context.MODE_PRIVATE);
		return prefs.getString(pref, defaultValue); 
	}
	
	// DELETE
	
	public static void deleteValue(Context context, String sharedPref){
		final SharedPreferences prefs = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
		final SharedPreferences.Editor editor = prefs.edit();
		editor.remove(sharedPref);
		editor.commit();
	}
	
}
