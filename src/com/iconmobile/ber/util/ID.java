
package com.iconmobile.ber.util;

public interface ID {
    
    public static final boolean DEBUG = false;

    public static final int CAMERA_HEIGHT = 480;
    public static final int CAMERA_WIDTH = 800;

    public static final int GROUND_HEIGHT = 65;

    public static final int HERO_HEIGHT = 90;
    public static final int HERO_WIDTH = 50;

    public static int FIXED_STEPS = 50;

    public static final String PATH_GRAPHICS = "gfx/";
    public static final String SUBPATH_GRAPHICS_MENU = "menu/";
    public static final String SUBPATH_GRAPHICS_GAME = "game/";
    public static final String SUBPATH_GRAPHICS_SPLASH = "splash/";
    public static final String SUBPATH_GRAPHICS_LOADING = "loading/";
    public static final String PATH_FONTS = "font/";
    public static final String PATH_LEVEL = "level/";
    public static final String PATH_SOUNDS = "mfx/";

    public static final String GRAPHIC_SPLASH = "splash_screen.png";
    
    public static final String GRAPHIC_LOADING = "loading_screen.png";
    
    public static final String GRAPHIC_MENU_BG = "bg_menu.png";
    public static final String GRAPHIC_MENU_BTN_PLAY = "btn_play.png";
    public static final String GRAPHIC_MENU_BTN_MUSIC = "btn_music.png";
    public static final String GRAPHIC_MENU_BTN_FB = "btn_facebook.png";
    public static final String GRAPHIC_MENU_BTN_ABOUT = "btn_about.png";
    public static final String GRAPHIC_MENU_OBJ_BALLOON = "obj_balloon.png";
    public static final String GRAPHIC_MENU_OBJ_PLANE = "obj_plane.png";
    public static final String GRAPHIC_MENU_OBJ_TRAIL = "obj_trail.png";
    public static final String GRAPHIC_MENU_CLOUDS_BACK = "clouds_back.png";
    public static final String GRAPHIC_MENU_CLOUDS_FRONT = "clouds_front.png";
    public static final String GRAPHIC_MENU_BG_BUILDINGS = "bg_building.png";
    public static final String GRAPHIC_MENU_CONSTRUCTION = "baustelle.png";
    public static final String GRAPHIC_IMPRINT_BG = "imprint_bg.png";
    public static final String GRAPHIC_ABOUT_IMPRINT_BTN = "btn_impressum.png";
    public static final String GRAPHIC_ABOUT_LICENCES_BTN = "btn_lizenzen.png";
    public static final String GRAPHIC_LICENCES_GPL_BTN = "btn_gpl.png";
    public static final String GRAPHIC_LICENCES_LGPL_BTN = "btn_lgpl.png";
    
    
    public static final String GRAPHIC_GAME_BTN_PAUSE = "btn_pause.png";
    public static final String GRAPHIC_GAME_MENU_BG = "menu_pause_bg.png";
    public static final String GRAPHIC_GAME_MENU_BTN_CONTINUE = "btn_continue.png";
    public static final String GRAPHIC_GAME_MENU_BTN_RESTART = "btn_restart.png";
    public static final String GRAPHIC_GAME_MENU_BTN_CANCEL = "btn_cancel.png";

    public static final String GRAPHIC_BG_00 = "bg_00.png";
    public static final String GRAPHIC_BG_01 = "bg_01.png";
    public static final String GRAPHIC_BG_02 = "bg_02.png";
    public static final String GRAPHIC_BG_03 = "bg_03.png";
    public static final String GRAPHIC_BG_04 = "bg_04.png";
    public static final String GRAPHIC_BG_05 = "bg_05.png";
    public static final String GRAPHIC_BG_06 = "bg_06.png";
    public static final String GRAPHIC_BG_07 = "bg_07.png";
    public static final String GRAPHIC_BG_08 = "bg_08.png";
    public static final String GRAPHIC_BG_09 = "bg_09.png";
    public static final String GRAPHIC_BG_10 = "bg_10.png";

    public static final String GRAPHIC_GROUND_TOP = "ground_top.png";
    public static final String GRAPHIC_GROUND_BOTTOM = "ground_bottom.png";
    
    public static final String GRAPHIC_STAGE_FRONT = "stage_front.png";
    public static final String GRAPHIC_STAGE_MIDDLE = "stage_middle.png";
    public static final String GRAPHIC_STAGE_BACK = "stage_back.png";

    public static final String GRAPHIC_HERO = "hero.png";
    public static final String GRAPHIC_SUIT = "suit.png";

    public static final String GRAPHIC_OBJ_TOWER = "obj_tower.png";
    public static final String GRAPHIC_OBJ_CONE = "obj_cone.png";
    public static final String GRAPHIC_OBJ_GLASS = "obj_glass.png";
    public static final String GRAPHIC_OBJ_PALLET = "obj_pallet.png";
    public static final String GRAPHIC_OBJ_TOOLBOX = "obj_toolbox.png";
    public static final String GRAPHIC_OBJ_PLANE = "obj_plane.png";
    public static final String GRAPHIC_OBJ_DIPPER = "obj_dipper.png";
    public static final String GRAPHIC_OBJ_FENCE = "obj_fence.png";
    public static final String GRAPHIC_OBJ_FLAG = "flag.png";
    public static final String GRAPHIC_OBJ_FLAG_POSTS = "flag_posts.png";
    
    public static final String GRAPHIC_SCORE_MINUS = "score_minus.png";
    public static final String GRAPHIC_SCORE_EURO = "score_euro.png";
    public static final String GRAPHIC_SCORE_LIFE = "life_pt.png";
    
    public static final String GRAPHIC_STORY_GAMEOVER_BG = "game_over_bg.png";
    public static final String GRAPHIC_STORY_BTN_DONATE = "btn_donate.png";
    public static final String GRAPHIC_STORY_BTN_SHARE = "btn_share.png";
    public static final String GRAPHIC_STORY_BUBBLE = "bubble.png";
    
    public static final String GRAPHIC_START_BEREIT = "start_bereit.png";
    public static final String GRAPHIC_START_UND = "start_und.png";
    public static final String GRAPHIC_START_LOS = "start_los.png";
    public static final String GRAPHIC_START_OK = "start_ok.png";

    public static final String GRAPHIC_TUTORIAL_AREA = "area_dash_line.png";
    public static final String GRAPHIC_TUTORIAL_ITEMS = "tutorial.png";
    
    public static final String FONT_8BIT = "8_BIT_WONDER.ttf";
    public static final String FONT_MUNRO = "munro.ttf";

    public static final String LEVEL_EXTENSION = ".xml";
    
    public static final String SOUND_MENU = "menu.ogg";
    public static final String SOUND_GAME = "game.ogg";

}
