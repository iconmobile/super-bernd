
package com.iconmobile.ber.view;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.input.touch.detector.ScrollDetector;
import org.andengine.input.touch.detector.ScrollDetector.IScrollDetectorListener;
import org.andengine.input.touch.detector.SurfaceScrollDetector;

import com.iconmobile.ber.R;
import com.iconmobile.ber.manager.ResourcesManager;
import com.iconmobile.ber.util.ID;

public class Imprint extends MenuScene {

    private ResourcesManager resourcesManager;
    public final static int CAMERA_WIDTH = ID.CAMERA_WIDTH;
    public final static int CAMERA_HEIGHT = ID.CAMERA_HEIGHT - 170;

    public Imprint(Camera camera) {
        super(camera);
        resourcesManager = ResourcesManager.getInstance();
        initImprint();
    }

    private void initImprint() {
        setBackgroundEnabled(false);

        Rectangle rect = new Rectangle(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT,
                resourcesManager.getVbom());
        rect.setColor(0, 0, 0);
        rect.setAlpha(0.5f);
        attachChild(rect);
        attachChild(new Sprite(32, -45, resourcesManager.imprint_bg_region, resourcesManager.getVbom()));
//        final String dialogString = Misc.getNormalizedText(resourcesManager.font_munro,
//                Misc.getImprintString(resourcesManager.getActivity()),
//                620);
        final String dialogString = resourcesManager.getActivity().getString(R.string.imprint);
        final Text dialog = new Text(90, 10, resourcesManager.font_munro,
                dialogString,
                resourcesManager.getVbom());
        dialog.setText(dialogString);
        dialog.setColor(0, 0, 0);
        attachChild(dialog);

        final ScrollDetector mScrollDetector = new SurfaceScrollDetector(new IScrollDetectorListener() {

            @Override
            public void onScrollStarted(ScrollDetector pScollDetector, int pPointerID, float pDistanceX, float pDistanceY) {
            }

            @Override
            public void onScroll(ScrollDetector pScollDetector, int pPointerID, float pDistanceX, float pDistanceY) {
                if (dialog.getY() + pDistanceY > 20) {
                    return;
                } else if (dialog.getY() + pDistanceY < -430) {
                    return;
                }
                dialog.setPosition(dialog.getX(), dialog.getY() + pDistanceY);
            }

            @Override
            public void onScrollFinished(ScrollDetector pScollDetector, int pPointerID, float pDistanceX, float pDistanceY) {
            }
        });
        mScrollDetector.setEnabled(true);
        setOnSceneTouchListener(new IOnSceneTouchListener() {

            @Override
            public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
                mScrollDetector.onSceneTouchEvent(pScene, pSceneTouchEvent);
                return true;
            }
        });
        setOnSceneTouchListenerBindingOnActionDownEnabled(true);
    }

}
