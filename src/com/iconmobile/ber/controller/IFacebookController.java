package com.iconmobile.ber.controller;

public interface IFacebookController {

    public boolean isValidSession();
    
    public void login();
    
    public void logout();
    
    public void post(String msg);
    
}
