
package com.iconmobile.ber.controller.scene;

import java.io.IOException;

import org.andengine.audio.music.exception.MusicReleasedException;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.input.touch.TouchEvent;
import org.andengine.util.debug.Debug;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.iconmobile.ber.controller.IFacebookController;
import com.iconmobile.ber.controller.scene.util.BaseScene;
import com.iconmobile.ber.manager.ActionManager;
import com.iconmobile.ber.manager.ActionManager.ContactType;
import com.iconmobile.ber.manager.ActionManager.IOnContactListener;
import com.iconmobile.ber.manager.LevelManager;
import com.iconmobile.ber.manager.SceneManager;
import com.iconmobile.ber.manager.SceneManager.SceneType;
import com.iconmobile.ber.manager.SharedPrefsManager;
import com.iconmobile.ber.manager.object.Hero;
import com.iconmobile.ber.util.ID;
import com.iconmobile.ber.view.GameInterface;
import com.iconmobile.ber.view.GameInterface.GameInterfaceType;
import com.iconmobile.ber.view.GameInterface.IOnGameInterfaceClickListener;
import com.iconmobile.ber.view.GameMenu;
import com.iconmobile.ber.view.GameMenu.GameMenuType;

public class GameScene extends BaseScene implements IOnMenuItemClickListener,
        IOnGameInterfaceClickListener, IOnContactListener {

    public static final String TAG = GameScene.class.getSimpleName();

    private GameInterface gameInterface;
    private GameMenu gameMenu;
    private PhysicsWorld physicsWorld;
    private Hero hero;
    private ActionManager contactManager;
    private LevelManager levelManager;

    private boolean chasingCamera;
    private boolean isPaused;

    private int score;

    @Override
    public void createScene() {
        score = 0;
        chasingCamera = false;
        isPaused = false;
        createPhysics();
        gameInterface = new GameInterface(this, (IFacebookController) activity);
        camera.setHUD(gameInterface);
        gameMenu = new GameMenu(camera);
        gameMenu.setOnMenuItemClickListener(this);
        contactManager = new ActionManager(this, physicsWorld, engine);
        contactManager.setOnContactListener(this);
        levelManager = new LevelManager(this, physicsWorld, camera);
        loadLevel("01");
        gameInterface.unregisterControl();
        gameInterface.hideScoreText();
        isPaused = true;
        if (SharedPrefsManager.getBooleanValue(activity,
                SharedPrefsManager.SHARED_PREF_FIRST_TIME,
                SharedPrefsManager.SHARED_PREF_FIRST_TIME_DEFAULT)) {
            gameInterface.tutorial();
        } else {
            initHero();
        }

        playMusic();
    }

    public void playMusic() {
        if (SharedPrefsManager.getBooleanValue(activity,
                SharedPrefsManager.SHARED_PREF_MUSIC,
                SharedPrefsManager.SHARED_PREF_MUSIC_DEFAULT)) {
            if (!resourcesManager.musicGame.isPlaying()) {
                resourcesManager.musicGame.play();
            }
        } else {
            if (resourcesManager.musicGame.isPlaying()) {
                resourcesManager.musicGame.pause();
                resourcesManager.musicGame.seekTo(0);
            }
        }
    }

    public void pauseMusic() {
        try {
            if (resourcesManager.musicGame.isPlaying()) {
                resourcesManager.musicGame.pause();
                resourcesManager.musicGame.seekTo(0);
            }
        } catch (MusicReleasedException e) {
        }
    }

    @Override
    public void onBackKeyPressed() {
        if (!isPaused) {
            pauseScene();
        } else {
            SceneManager.getInstance().loadMenuScene(engine);
        }
    }

    @Override
    public SceneType getSceneType() {
        return SceneType.SCENE_GAME;
    }

    @Override
    public void disposeScene() {
        camera.clearUpdateHandlers();
        camera.setHUD(null);
        camera.setCenterDirect(ID.CAMERA_WIDTH / 2, ID.CAMERA_HEIGHT / 2);
        camera.setChaseEntity(null);
        unregisterUpdateHandler(physicsWorld);
        if (resourcesManager.musicGame.isPlaying()) {
            resourcesManager.musicGame.pause();
            resourcesManager.musicGame.seekTo(0);
        }
    }

    public void pauseScene() {
        isPaused = true;
        gameInterface.unregisterControl();
        gameInterface.hideLife();
        SceneManager.getInstance().getCurrentScene().setIgnoreUpdate(true);
        gameInterface.hideScoreText();
        setChildScene(gameMenu);
        try {
            if (resourcesManager.musicGame.isPlaying()) {
                resourcesManager.musicGame.pause();
            }
        } catch (MusicReleasedException e) {
        }
    }

    private void continueScene() {
        isPaused = false;
        gameInterface.registerControl();
        gameInterface.showLife(SharedPrefsManager.getIntegerValue(activity,
                SharedPrefsManager.SHARED_PREF_LIFES,
                SharedPrefsManager.SHARED_PREF_LIFES_DEFAULT));
        gameInterface.setScoreText(score);
        SceneManager.getInstance().getCurrentScene().setIgnoreUpdate(false);
        clearChildScene();
        if (!resourcesManager.musicGame.isPlaying() &&
                SharedPrefsManager.getBooleanValue(activity,
                        SharedPrefsManager.SHARED_PREF_MUSIC,
                        SharedPrefsManager.SHARED_PREF_MUSIC_DEFAULT)) {
            resourcesManager.musicGame.play();
        }
    }

    private void createPhysics() {
        physicsWorld = new FixedStepPhysicsWorld(ID.FIXED_STEPS, new Vector2(0,
                50), false);
        physicsWorld.setContinuousPhysics(true);

        registerUpdateHandler(new IUpdateHandler() {

            @Override
            public void reset() {
            }

            @Override
            public void onUpdate(float pSecondsElapsed) {
                if (hero != null && chasingCamera)
                    camera.setCenterDirect(hero.getX() + 280, hero.getY() - 100);
            }
        });

        registerUpdateHandler(physicsWorld);
    }

    private void loadLevel(String levelID) {
        levelManager.setAssetBasePath(ID.PATH_LEVEL);
        try {
            levelManager.loadLevelFromAsset(activity.getAssets(), levelID
                    + ID.LEVEL_EXTENSION);
        } catch (final IOException e) {
            Debug.e(e);
        }
        camera.setBounds(0, 0, levelManager.getLevelWidth(), ID.CAMERA_HEIGHT);
        camera.setBoundsEnabled(true);
    }

    private void initHero() {
        hero = new Hero(-ID.HERO_WIDTH, ID.CAMERA_HEIGHT - ID.GROUND_HEIGHT
                - ID.HERO_HEIGHT, vbom, physicsWorld);
        attachChild(hero);

        final TimerHandler stop = new TimerHandler(0.5f, new ITimerCallback() {
            public void onTimePassed(final TimerHandler pTimerHandler) {
                unregisterUpdateHandler(pTimerHandler);
                hero.stop();
                if (SharedPrefsManager.getIntegerValue(activity,
                        SharedPrefsManager.SHARED_PREF_LIFES,
                        SharedPrefsManager.SHARED_PREF_LIFES_DEFAULT)
                == SharedPrefsManager.SHARED_PREF_LIFES_DEFAULT) {
                    gameInterface.intro();
                } else {
                    gameInterface.skipIntro();
                }
            }
        });
        final TimerHandler moveIn = new TimerHandler(1f,
                new ITimerCallback() {
                    public void onTimePassed(final TimerHandler pTimerHandler) {
                        unregisterUpdateHandler(pTimerHandler);
                        registerUpdateHandler(stop);
                        chasingCamera = true;
                        camera.setMaxVelocityX(10000);
                        hero.moveSlow();
                    }
                });
        registerUpdateHandler(moveIn);
    }

    private void startGame() {
        hero.moveMedium();
        gameInterface.registerControl();
        gameInterface.setScoreText(score);
        gameInterface.showLife(SharedPrefsManager.getIntegerValue(activity,
                SharedPrefsManager.SHARED_PREF_LIFES,
                SharedPrefsManager.SHARED_PREF_LIFES_DEFAULT));
        gameInterface.go();
        physicsWorld.setContactListener(contactManager);
        isPaused = false;
    }

    @Override
    public void onOnGameInterfaceClicked(GameInterfaceType type,
            TouchEvent touchEvent, float x, float y) {
        switch (type) {
            case JUMP:
                if (touchEvent.isActionDown()) {
                    if (!hero.jump()) {
                        final TimerHandler jumpBuffer = new TimerHandler(0.08f,
                                new ITimerCallback() {
                                    @Override
                                    public void onTimePassed(
                                            TimerHandler pTimerHandler) {
                                        GameScene.this
                                                .unregisterUpdateHandler(pTimerHandler);
                                        hero.jump();
                                    }
                                });
                        GameScene.this.registerUpdateHandler(jumpBuffer);
                    }
                }
                break;
            case CROUCH:
                if (touchEvent.isActionDown()) {
                    hero.crouch();
                }
                if (touchEvent.isActionUp()) {
                    hero.run();
                }
                break;
            case PAUSE:
                pauseScene();
                break;
            case RESTART:
                restartScene();
                break;
            case SHARE:
                if (((IFacebookController) activity).isValidSession()) {
                    ((IFacebookController) activity).post(null);
                }
                break;
            case START:
                startGame();
                break;
            case END:
                SceneManager.getInstance().loadMenuScene(engine);
                break;
            case TUTORIAL_END:
                SharedPrefsManager.setBooleanValue(activity,
                        SharedPrefsManager.SHARED_PREF_FIRST_TIME,
                        false);
                initHero();
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem,
            float pMenuItemLocalX, float pMenuItemLocalY) {
        switch (pMenuItem.getID()) {
            case GameMenuType.CONTINUE:
                if (hasChildScene()) {
                    continueScene();
                }
                return true;
            case GameMenuType.RESTART:
                SharedPrefsManager.setIntegerValue(activity,
                        SharedPrefsManager.SHARED_PREF_LIFES,
                        SharedPrefsManager.SHARED_PREF_LIFES_DEFAULT);
                restartScene();
                return true;
            case GameMenuType.CANCEL:
                SharedPrefsManager.setIntegerValue(activity,
                        SharedPrefsManager.SHARED_PREF_LIFES,
                        SharedPrefsManager.SHARED_PREF_LIFES_DEFAULT);
                SceneManager.getInstance().loadMenuScene(engine);
                return true;
            default:
                return false;
        }
    }

    private void restartScene() {
        SceneManager.getInstance().loadGameScene(engine);
    }

    @Override
    public void onContact(ContactType type, Body b1, Body b2) {
        int lifes;
        switch (type) {
            case BEGIN_HERO_GROUND:
                hero.setFoodContact(true);
                if (!hero.isCrouching()) {
                    hero.run();
                }
                break;
            case END_HERO_GROUND:
                hero.setFoodContact(false);
                break;
            case BEGIN_HERO_COIN:
                addToScore(10);
                break;
            case BEGIN_HERO_BOX:
                addToScore(-10);
                break;
            case BEGIN_HERO_STAGE:
                hero.crash();
                chasingCamera = false;
                camera.setMaxVelocityX(510);
                camera.setCenter(hero.getX() + 1000, hero.getY() - 100);
                registerUpdateHandler(new IUpdateHandler() {

                    @Override
                    public void onUpdate(float pSecondsElapsed) {
                        if (camera.getMaxVelocityX() > 0)
                            camera.setMaxVelocityX(camera.getMaxVelocityX() - 30);
                    }

                    @Override
                    public void reset() {
                    }

                });
                gameInterface.unregisterControl();
                isPaused = true;
                lifes = SharedPrefsManager.getIntegerValue(activity,
                        SharedPrefsManager.SHARED_PREF_LIFES,
                        SharedPrefsManager.SHARED_PREF_LIFES_DEFAULT);
                gameInterface.showLife(lifes - 1);
                if (lifes == 1) {
                    SharedPrefsManager.setIntegerValue(activity,
                            SharedPrefsManager.SHARED_PREF_LIFES,
                            SharedPrefsManager.SHARED_PREF_LIFES_DEFAULT);
                    registerUpdateHandler(new TimerHandler(0.5f,
                            new ITimerCallback() {
                                public void onTimePassed(final TimerHandler pTimerHandler) {
                                    gameInterface.hideLife();
                                    gameInterface.hideScoreText();
                                    gameInterface.gameover();
                                }
                            }));
                } else {
                    SharedPrefsManager.setIntegerValue(activity,
                            SharedPrefsManager.SHARED_PREF_LIFES,
                            lifes - 1);
                    registerUpdateHandler(new TimerHandler(0.5f,
                            new ITimerCallback() {
                                public void onTimePassed(final TimerHandler pTimerHandler) {
                                    restartScene();
                                }
                            }));
                }
                break;
            case BEGIN_HERO_FINISH:
                gameInterface.unregisterControl();
                if (!isPaused) {
                    isPaused = true;
                    final boolean good;
                    if (score >= levelManager.getPerfectScore()) {
                        good = true;
                        SharedPrefsManager.setIntegerValue(activity,
                                SharedPrefsManager.SHARED_PREF_LIFES,
                                SharedPrefsManager.SHARED_PREF_LIFES_DEFAULT);
                    } else if (score < 0) {
                        good = false;
                        lifes = SharedPrefsManager.getIntegerValue(activity,
                                SharedPrefsManager.SHARED_PREF_LIFES,
                                SharedPrefsManager.SHARED_PREF_LIFES_DEFAULT);
                        if (lifes == 1) {
                            SharedPrefsManager.setIntegerValue(activity,
                                    SharedPrefsManager.SHARED_PREF_LIFES,
                                    SharedPrefsManager.SHARED_PREF_LIFES_DEFAULT);
                            registerUpdateHandler(new TimerHandler(0.5f,
                                    new ITimerCallback() {
                                        public void onTimePassed(final TimerHandler pTimerHandler) {
                                            gameInterface.hideLife();
                                            gameInterface.hideScoreText();
                                            gameInterface.gameover();
                                        }
                                    }));
                            break;
                        }
                        SharedPrefsManager.setIntegerValue(activity,
                                SharedPrefsManager.SHARED_PREF_LIFES,
                                SharedPrefsManager.getIntegerValue(activity,
                                        SharedPrefsManager.SHARED_PREF_LIFES,
                                        SharedPrefsManager.SHARED_PREF_LIFES_DEFAULT) - 1);
                    } else {
                        good = true;
                        SharedPrefsManager.setIntegerValue(activity,
                                SharedPrefsManager.SHARED_PREF_LIFES,
                                SharedPrefsManager.SHARED_PREF_LIFES_DEFAULT);
                    }
                    registerUpdateHandler(new TimerHandler(0.3f, new ITimerCallback() {
                        @Override
                        public void onTimePassed(TimerHandler pTimerHandler) {
                            unregisterUpdateHandler(pTimerHandler);
                            detachChild(hero);
                            gameInterface.outro(good);
                        }
                    }));
                }
                break;
            default:
                break;
        }
    }

    private void addToScore(int i) {
        score += i;
        gameInterface.setScoreText(score);
    }

    @Override
    public void updateUi() {
    }
    
    public boolean isPaused() {
        return isPaused;
    }

}
