package com.iconmobile.ber.controller.scene;

import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.util.color.Color;

import com.iconmobile.ber.controller.scene.util.BaseScene;
import com.iconmobile.ber.manager.SceneManager.SceneType;
import com.iconmobile.ber.manager.SharedPrefsManager;

public class LoadingScene extends BaseScene {

	@Override
	public void createScene() {
		setBackground(new Background(Color.BLACK));
		attachChild(new Sprite(0, 0, resourcesManager.loading_region, vbom));
		
		SharedPrefsManager.setIntegerValue(activity, 
		        SharedPrefsManager.SHARED_PREF_LIFES, 
		        SharedPrefsManager.SHARED_PREF_LIFES_DEFAULT);
	}

	@Override
	public void onBackKeyPressed() {
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_LOADING;
	}

	@Override
	public void disposeScene() {
	}

    @Override
    public void updateUi() {
        // TODO Auto-generated method stub
        
    }

}
