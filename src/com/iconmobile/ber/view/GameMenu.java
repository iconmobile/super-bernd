
package com.iconmobile.ber.view;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.item.AnimatedSpriteMenuItem;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.sprite.Sprite;

import com.iconmobile.ber.manager.ResourcesManager;
import com.iconmobile.ber.util.ID;

public class GameMenu extends MenuScene {

    public class GameMenuType {
        public final static int CONTINUE = 0;
        public final static int RESTART = 1;
        public final static int CANCEL = 2;
    }

    private ResourcesManager resourcesManager;

    public GameMenu(Camera camera) {
        super(camera);
        resourcesManager = ResourcesManager.getInstance();
        initGameMenu();
    }

    private void initGameMenu() {
        setBackgroundEnabled(false);

        Rectangle rect = new Rectangle(0, 0, ID.CAMERA_WIDTH, ID.CAMERA_HEIGHT,
                resourcesManager.getVbom());
        rect.setColor(0, 0, 0);
        rect.setAlpha(0.5f);
        attachChild(rect);
        attachChild(new Sprite(32, 25, resourcesManager.game_menu_bg_region, resourcesManager.getVbom()));

        final IMenuItem continueMenuItem = new AnimatedSpriteMenuItem(GameMenuType.CONTINUE,
                resourcesManager.game_menu_continue_region,
                resourcesManager.getVbom()) {
            @Override
            public void onSelected() {
                super.onSelected();
                this.setCurrentTileIndex(1);
            }

            @Override
            public void onUnselected() {
                super.onUnselected();
                this.setCurrentTileIndex(0);
            }

        };
        continueMenuItem.setPosition(198, 75);
        addMenuItem(continueMenuItem);
        registerTouchArea(continueMenuItem);

        final IMenuItem restartMenuItem = new AnimatedSpriteMenuItem(GameMenuType.RESTART,
                resourcesManager.game_menu_restart_region,
                resourcesManager.getVbom()) {
            @Override
            public void onSelected() {
                super.onSelected();
                this.setCurrentTileIndex(1);
            }

            @Override
            public void onUnselected() {
                super.onUnselected();
                this.setCurrentTileIndex(0);
            }

        };
        restartMenuItem.setPosition(continueMenuItem.getX(), continueMenuItem.getY() + continueMenuItem.getHeight() + 6);
        addMenuItem(restartMenuItem);
        registerTouchArea(restartMenuItem);

        final IMenuItem cancelMenuItem = new AnimatedSpriteMenuItem(GameMenuType.CANCEL,
                resourcesManager.game_menu_cancel_region,
                resourcesManager.getVbom()) {
            @Override
            public void onSelected() {
                super.onSelected();
                this.setCurrentTileIndex(1);
            }

            @Override
            public void onUnselected() {
                super.onUnselected();
                this.setCurrentTileIndex(0);
            }

        };
        cancelMenuItem.setPosition(restartMenuItem.getX(),  restartMenuItem.getY() + restartMenuItem.getHeight() + 6);
        addMenuItem(cancelMenuItem);
        registerTouchArea(cancelMenuItem);
    }

}
