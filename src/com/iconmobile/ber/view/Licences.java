
package com.iconmobile.ber.view;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.item.AnimatedSpriteMenuItem;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.sprite.Sprite;

import com.iconmobile.ber.manager.ResourcesManager;
import com.iconmobile.ber.util.ID;

public class Licences extends MenuScene {

    private ResourcesManager resourcesManager;
    public final static int CAMERA_WIDTH = ID.CAMERA_WIDTH;
    public final static int CAMERA_HEIGHT = ID.CAMERA_HEIGHT;
    
    public class LicencesMenuType {
        public final static int GPL = 20;
        public final static int LGPL = 21;
    }

    public Licences(Camera camera) {
        super(camera);
        resourcesManager = ResourcesManager.getInstance();
        initAbout();
    }

    private void initAbout() {
        setBackgroundEnabled(false);

        Rectangle rect = new Rectangle(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT,
                resourcesManager.getVbom());
        rect.setColor(0, 0, 0);
        rect.setAlpha(0.5f);
        attachChild(rect);
        attachChild(new Sprite(32, 25, resourcesManager.imprint_bg_region, resourcesManager.getVbom()));

        final IMenuItem gplMenuItem = new AnimatedSpriteMenuItem(LicencesMenuType.GPL,
                resourcesManager.licences_gpl_region, 
                resourcesManager.getVbom()) {
            @Override
            public void onSelected() {
                super.onSelected();
                this.setCurrentTileIndex(1);
            }

            @Override
            public void onUnselected() {
                super.onUnselected();
                this.setCurrentTileIndex(0);
            }

        };
        gplMenuItem.setPosition((CAMERA_WIDTH - gplMenuItem.getWidth()) / 2, 130);
        addMenuItem(gplMenuItem);
        registerTouchArea(gplMenuItem);

        final IMenuItem lgplMenuItem = new AnimatedSpriteMenuItem(LicencesMenuType.LGPL,
                resourcesManager.licences_lgpl_region, 
                resourcesManager.getVbom()) {
            @Override
            public void onSelected() {
                super.onSelected();
                this.setCurrentTileIndex(1);
            }

            @Override
            public void onUnselected() {
                super.onUnselected();
                this.setCurrentTileIndex(0);
            }

        };
        lgplMenuItem.setPosition(gplMenuItem.getX(), gplMenuItem.getY() + lgplMenuItem.getHeight() + 6);
        addMenuItem(lgplMenuItem);
        registerTouchArea(lgplMenuItem);
    }

}
