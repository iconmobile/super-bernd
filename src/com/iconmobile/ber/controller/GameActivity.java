
package com.iconmobile.ber.controller;

import org.andengine.engine.Engine;
import org.andengine.engine.LimitedFPSEngine;
import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.BaseGameActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.util.Log;

import com.iconmobile.ber.controller.scene.GameScene;
import com.iconmobile.ber.controller.scene.MainMenuScene;
import com.iconmobile.ber.manager.ResourcesManager;
import com.iconmobile.ber.manager.SceneManager;
import com.iconmobile.ber.util.ID;

public class GameActivity extends BaseGameActivity implements IFacebookController {

    public static final String TAG = BaseGameActivity.class.getSimpleName();

    private SmoothCamera camera;
    private SceneManager sceneManager;

    @Override
    public Engine onCreateEngine(EngineOptions pEngineOptions) {
        Log.d(TAG, "onCreateEngine()");
        return new LimitedFPSEngine(pEngineOptions, ID.FIXED_STEPS);
    }

    @Override
    public EngineOptions onCreateEngineOptions() {
        Log.d(TAG, "onCreateEngineOptions()");
        camera = new SmoothCamera(0, 0, ID.CAMERA_WIDTH, ID.CAMERA_HEIGHT, 0, 0, 0);
        // EngineOptions engineOptions = new EngineOptions(true,
        // ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(
        // ID.CAMERA_WIDTH, ID.CAMERA_HEIGHT), this.camera);
        EngineOptions engineOptions = new EngineOptions(true,
                ScreenOrientation.LANDSCAPE_SENSOR, new FillResolutionPolicy(), this.camera);
        engineOptions.getAudioOptions().setNeedsMusic(true).setNeedsSound(true);
        engineOptions.getTouchOptions().setNeedsMultiTouch(true);
        engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
        engineOptions.setUpdateThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_DISPLAY);
        return engineOptions;
    }

    @Override
    public void onCreateResources(
            OnCreateResourcesCallback pOnCreateResourcesCallback)
            throws Exception {
        Log.d(TAG, "onCreateResources()");
        ResourcesManager.prepareManager(mEngine, this, camera,
                getVertexBufferObjectManager());
        pOnCreateResourcesCallback.onCreateResourcesFinished();
    }

    @Override
    public void onReloadResources() {
        super.onReloadResources();
        ResourcesManager.prepareManager(mEngine, this, camera,
                getVertexBufferObjectManager());
    }

    @Override
    public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback)
            throws Exception {
        Log.d(TAG, "onCreateScene()");
        sceneManager = SceneManager.getInstance();
        sceneManager.createSplashScene(pOnCreateSceneCallback);
    }

    @Override
    public void onPopulateScene(Scene pScene,
            OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception {
        Log.d(TAG, "onPopulateScene()");
        mEngine.registerUpdateHandler(new TimerHandler(2f,
                new ITimerCallback() {

                    public void onTimePassed(final TimerHandler pTimerHandler) {
                        mEngine.unregisterUpdateHandler(pTimerHandler);
                        sceneManager.createMenuScene();
                    }
                }));
        pOnPopulateSceneCallback.onPopulateSceneFinished();
    }

    @Override
    public void onBackPressed() {
        SceneManager.getInstance().getCurrentScene().onBackKeyPressed();
    }

    @Override
    public void onConfigurationChanged(Configuration pNewConfig) {
        Log.d(TAG, "onConfigurationChanged() called");
        // Insert code to analyze the new config and make changes to your application
        super.onConfigurationChanged(pNewConfig);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");

    }

    @Override
    public synchronized void onResumeGame() {
        super.onResumeGame();
        Log.d(TAG, "onResumeGame()");
        if (SceneManager.getInstance().getCurrentScene() != null) {
            SceneManager.getInstance().getCurrentScene().updateUi();
            if (SceneManager.getInstance().getCurrentScene() instanceof GameScene) {
                ((GameScene) SceneManager.getInstance().getCurrentScene()).playMusic();
            } else if (SceneManager.getInstance().getCurrentScene() instanceof MainMenuScene) {
                ((MainMenuScene) SceneManager.getInstance().getCurrentScene()).playMusic();
            }
        }
        registerReceiver(receiver, new IntentFilter(Intent.ACTION_BATTERY_LOW));
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause()");
        super.onPause();
        if (SceneManager.getInstance().getCurrentScene() != null) {
            if (SceneManager.getInstance().getCurrentScene() instanceof GameScene) {
                ((GameScene) SceneManager.getInstance().getCurrentScene()).pauseMusic();
                if (!((GameScene) SceneManager.getInstance().getCurrentScene()).isPaused()) {
                    ((GameScene) SceneManager.getInstance().getCurrentScene()).pauseScene();
                }
            } else if (SceneManager.getInstance().getCurrentScene() instanceof MainMenuScene) {
                ((MainMenuScene) SceneManager.getInstance().getCurrentScene()).pauseMusic();
            }
        }
        unregisterReceiver(receiver);
    }

    @Override
    public synchronized void onPauseGame() {
        super.onPauseGame();
        Log.d(TAG, "onPauseGame()");
    }

    @Override
    public boolean isValidSession() {
        // TODO Facebook Session Check
        return false;
    }

    @Override
    public void login() {
        // TODO Facebook Login
    }

    @Override
    public void logout() {
        // TODO Facebook Logout
    }

    @Override
    public void post(String msg) {
        // TODO Facebook Status Post
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (SceneManager.getInstance().getCurrentScene() instanceof GameScene) {
                if (!((GameScene) SceneManager.getInstance().getCurrentScene()).isPaused()) {
                    ((GameScene) SceneManager.getInstance().getCurrentScene()).pauseScene();
                }
            }
        }
    };

}
