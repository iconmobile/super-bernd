package com.iconmobile.ber.controller.scene;

import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.util.color.Color;

import com.iconmobile.ber.controller.scene.util.BaseScene;
import com.iconmobile.ber.manager.SceneManager.SceneType;

public class SplashScene extends BaseScene {

	private Sprite splash;

	@Override
	public void createScene() {
		createBackground();
		splash = new Sprite(0, 0, resourcesManager.splash_region, vbom);
		attachChild(splash);
	}

	@Override
	public void onBackKeyPressed() {
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_SPLASH;
	}

	@Override
	public void disposeScene() {
		splash.detachSelf();
		splash.dispose();
		this.detachSelf();
		this.dispose();
	}

	private void createBackground() {
		setBackground(new Background(Color.WHITE));
	}

    @Override
    public void updateUi() {
    }

}
