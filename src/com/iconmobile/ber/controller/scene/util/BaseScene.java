package com.iconmobile.ber.controller.scene.util;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.SmoothCamera;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.app.Activity;

import com.iconmobile.ber.manager.ResourcesManager;
import com.iconmobile.ber.manager.SceneManager.SceneType;

public abstract class BaseScene extends Scene {

	// Attributes
	
	protected Engine engine;
	protected Activity activity;
	protected ResourcesManager resourcesManager;
	protected VertexBufferObjectManager vbom;
	protected SmoothCamera camera;

	// Constructor

	public BaseScene() {
		this.resourcesManager = ResourcesManager.getInstance();
		this.engine = resourcesManager.getEngine();
		this.activity = resourcesManager.getActivity();
		this.vbom = resourcesManager.getVbom();
		this.camera = resourcesManager.getCamera();
		createScene();
	}

	// Methods

	public abstract void createScene();

	public abstract void onBackKeyPressed();
	
	public abstract void updateUi();
	
	public abstract SceneType getSceneType();

	public abstract void disposeScene();

}
