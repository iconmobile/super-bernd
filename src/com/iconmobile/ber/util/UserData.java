package com.iconmobile.ber.util;

import org.andengine.entity.shape.IShape;

public class UserData {

	public static enum Type {
		HERO, GROUND, CONE, FINISH, TOOLBOX, STAGE, GLASS, PALLET
	}
	
	private Type type;
	private IShape shape;

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public IShape getShape() {
		return shape;
	}

	public void setShape(IShape shape) {
		this.shape = shape;
	}

	public UserData(Type type, IShape shape) {
		super();
		this.type = type;
		this.shape = shape;
	}

}
