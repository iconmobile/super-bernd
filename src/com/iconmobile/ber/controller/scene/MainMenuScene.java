
package com.iconmobile.ber.controller.scene;

import org.andengine.audio.music.exception.MusicReleasedException;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.PathModifier;
import org.andengine.entity.modifier.PathModifier.Path;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.util.GLState;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.iconmobile.ber.controller.IFacebookController;
import com.iconmobile.ber.controller.scene.util.BaseScene;
import com.iconmobile.ber.manager.SceneManager;
import com.iconmobile.ber.manager.SceneManager.SceneType;
import com.iconmobile.ber.manager.SharedPrefsManager;
import com.iconmobile.ber.util.ID;
import com.iconmobile.ber.view.About;
import com.iconmobile.ber.view.About.AboutMenuType;
import com.iconmobile.ber.view.Imprint;
import com.iconmobile.ber.view.Licences;
import com.iconmobile.ber.view.Licences.LicencesMenuType;
import com.iconmobile.ber.view.MainMenu;
import com.iconmobile.ber.view.MainMenu.MainMenuType;
import com.iconmobile.ber.view.util.ParallaxLayer;
import com.iconmobile.ber.view.util.ParallaxLayer.ParallaxEntity;

public class MainMenuScene extends BaseScene implements
        IOnMenuItemClickListener {

    private MainMenu mainMenu;
    private Imprint imprint;
    private About about;
    private Licences licences;
    private int currentSubmenu;
    private final static int SUBMENU_NONE = 0;
    private final static int SUBMENU_ABOUT = 1;
    private final static int SUBMENU_IMPRINT = 2;
    private final static int SUBMENU_LICENCES = 3;

    @Override
    public void createScene() {
        createBackground();
        createMenuChildScene();
        playMusic();
        currentSubmenu = SUBMENU_NONE;
        imprint = new Imprint(new SmoothCamera(0, 0, Imprint.CAMERA_WIDTH, Imprint.CAMERA_HEIGHT, 0, 0, 0));
        about = new About(camera);
        licences = new Licences(camera);
    }

    public void playMusic() {
        if (SharedPrefsManager.getBooleanValue(activity,
                SharedPrefsManager.SHARED_PREF_MUSIC,
                SharedPrefsManager.SHARED_PREF_MUSIC_DEFAULT)) {
            if (!resourcesManager.musicMenu.isPlaying()) {
                resourcesManager.musicMenu.play();
            }
        } else {
            if (resourcesManager.musicMenu.isPlaying()) {
                resourcesManager.musicMenu.pause();
                resourcesManager.musicMenu.seekTo(0);
            }
        }
    }

    public void pauseMusic() {
        try {
            if (resourcesManager != null && resourcesManager.musicMenu != null) {
                if (resourcesManager.musicMenu.isPlaying()) {
                    resourcesManager.musicMenu.pause();
                    resourcesManager.musicMenu.seekTo(0);
                }
            }
        } catch (MusicReleasedException e) {
        }
    }

    @Override
    public void onBackKeyPressed() {
        switch (currentSubmenu) {
            case SUBMENU_ABOUT:
                hideAbout();
                break;
            case SUBMENU_IMPRINT:
                hideImprint();
                break;
            case SUBMENU_LICENCES:
                hideLicences();
                break;
            case SUBMENU_NONE:
                System.exit(0);
                break;
            default:
                System.exit(0);
                break;
        }
    }

    @Override
    public SceneType getSceneType() {
        return SceneType.SCENE_MENU;
    }

    @Override
    public void disposeScene() {
        if (resourcesManager.musicMenu.isPlaying()) {
            resourcesManager.musicMenu.pause();
            resourcesManager.musicMenu.seekTo(0);
        }
    }

    private void createBackground() {
        attachChild(new Sprite(0, 0,
                resourcesManager.menu_background_region, vbom) {
            @Override
            protected void preDraw(GLState pGLState, Camera pCamera) {
                super.preDraw(pGLState, pCamera);
                pGLState.enableDither();
            }
        });

        ParallaxLayer parallaxBackground = new ParallaxLayer(camera,
                false);
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(-0.99f,
                new Sprite(0, 50,
                        resourcesManager.menu_clouds_back,
                        resourcesManager.getVbom()), false));
        parallaxBackground.setParallaxChangePerSecond(8);
        attachChild(parallaxBackground);

        createObjects();
        attachChild(new Sprite(0, 0,
                resourcesManager.menu_bg_buildings,
                resourcesManager.getVbom()));

        parallaxBackground = new ParallaxLayer(camera,
                false);
        parallaxBackground.attachParallaxEntity(new ParallaxEntity(-0.96f,
                new Sprite(0, 110,
                        resourcesManager.menu_clouds_front,
                        resourcesManager.getVbom()), false));
        parallaxBackground.setParallaxChangePerSecond(8);
        attachChild(parallaxBackground);
    }

    private void createObjects() {
        int x = 165;
        int y_up = 120;
        int y_down = 200;
        Sprite balloon = new Sprite(x, ID.CAMERA_HEIGHT,
                resourcesManager.menu_obj_balloon,
                resourcesManager.getVbom());
        balloon.registerEntityModifier(
                new SequenceEntityModifier(
                        new PathModifier(2f, new Path(2).to(x, ID.CAMERA_HEIGHT + 10).to(x, ID.CAMERA_HEIGHT)),
                        new PathModifier(20f, new Path(2).to(x, ID.CAMERA_HEIGHT).to(x, y_up)),
                        new LoopEntityModifier(
                                new SequenceEntityModifier(
                                        new PathModifier(10f, new Path(2).to(x, y_up).to(x, y_down)),
                                        new PathModifier(10f, new Path(2).to(x, y_down).to(x, y_up))))));
        attachChild(balloon);

        int y = 50;
        Sprite plane = new Sprite(ID.CAMERA_WIDTH, y,
                resourcesManager.menu_obj_plane,
                resourcesManager.getVbom());
        plane.registerEntityModifier(
                new SequenceEntityModifier(
                        new PathModifier(5f, new Path(2).to(ID.CAMERA_WIDTH + 10, y).to(ID.CAMERA_WIDTH, y)),
                        new PathModifier(20f, new Path(2).to(ID.CAMERA_WIDTH, y).to(-plane.getWidth(), y))));
        attachChild(plane);

        y = y + 19;
        AnimatedSprite trials = new AnimatedSprite(ID.CAMERA_WIDTH + plane.getWidth(), y,
                resourcesManager.menu_obj_trail,
                resourcesManager.getVbom());
        trials.registerEntityModifier(
                new SequenceEntityModifier(
                        new PathModifier(5f, new Path(2).to(ID.CAMERA_WIDTH + plane.getWidth() + 10, y).to(ID.CAMERA_WIDTH + plane.getWidth(), y)),
                        new PathModifier(20f, new Path(2)
                                .to(ID.CAMERA_WIDTH + plane.getWidth(), y)
                                .to(0, y)),
                        new PathModifier(30f, new Path(2)
                                .to(0, y)
                                .to(-trials.getWidth(), y))));
        trials.animate(500);
        attachChild(trials);
    }

    private void createMenuChildScene() {
        mainMenu = new MainMenu(activity, camera, (IFacebookController) activity);
        mainMenu.setOnMenuItemClickListener(this);
        setChildScene(mainMenu);
    }

    @Override
    public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem,
            float pMenuItemLocalX, float pMenuItemLocalY) {
        Log.i("onMenutItemClicked", ""+pMenuItem.getID());
        IFacebookController fb = (IFacebookController) activity;
        switch (pMenuItem.getID()) {
            case MainMenuType.PLAY:
                SceneManager.getInstance().loadGameScene(engine);
                return true;
            case MainMenuType.MUSIC:
                SharedPrefsManager.setBooleanValue(activity,
                        SharedPrefsManager.SHARED_PREF_MUSIC,
                        !SharedPrefsManager.getBooleanValue(activity,
                                SharedPrefsManager.SHARED_PREF_MUSIC,
                                SharedPrefsManager.SHARED_PREF_MUSIC_DEFAULT));
                playMusic();
                return true;
            case MainMenuType.FACEBOOK_AUTH:
                final Sprite cons = new Sprite(242, 237,
                        resourcesManager.text_construction,
                        resourcesManager.getVbom());
                attachChild(cons);
                registerUpdateHandler(new TimerHandler(0.2f,
                        new ITimerCallback() {
                            private int counter = 0;

                            public void onTimePassed(final TimerHandler pTimerHandler) {
                                counter++;
                                cons.setVisible(!cons.isVisible());
                                if (counter == 3) {
                                    MainMenuScene.this.unregisterUpdateHandler(pTimerHandler);
                                } else {
                                    pTimerHandler.reset();
                                }
                            }
                        }));
                if (fb.isValidSession()) {
                    fb.logout();
                } else {
                    fb.login();
                }
                return true;
            case MainMenuType.ABOUT:
                // final Sprite cons1 = new Sprite(242, 237,
                // resourcesManager.text_construction,
                // resourcesManager.getVbom());
                // attachChild(cons1);
                // registerUpdateHandler(new TimerHandler(0.2f,
                // new ITimerCallback() {
                // private int counter = 0;
                //
                // public void onTimePassed(final TimerHandler pTimerHandler) {
                // counter++;
                // cons1.setVisible(!cons1.isVisible());
                // if (counter == 3) {
                // MainMenuScene.this.unregisterUpdateHandler(pTimerHandler);
                // } else {
                // pTimerHandler.reset();
                // }
                // }
                // }));
                showAbout();
                return true;
            case AboutMenuType.IMPRINT:
                showImprint();
                return true;
            case AboutMenuType.LICENCES:
                showLicences();
                return true;
            case LicencesMenuType.GPL:
                Intent browserGPLIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.gnu.org/licenses/gpl.txt"));
                resourcesManager.getActivity().startActivity(browserGPLIntent);
                return true;
            case LicencesMenuType.LGPL:
                Intent browserLGPLIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.gnu.org/licenses/lgpl.txt"));
                resourcesManager.getActivity().startActivity(browserLGPLIntent);
                return true;
            default:
                return false;
        }
    }

    private void showAbout() {
        setChildScene(about);
        about.setOnMenuItemClickListener(this);
        currentSubmenu = SUBMENU_ABOUT;
    }

    private void hideAbout() {
        clearChildScene();
        createMenuChildScene();
        currentSubmenu = SUBMENU_NONE;
    }

    private void showImprint() {
        setChildScene(imprint);
        imprint.setOnMenuItemClickListener(this);
        currentSubmenu = SUBMENU_IMPRINT;
    }

    private void hideImprint() {
        clearChildScene();
        showAbout();
        currentSubmenu = SUBMENU_ABOUT;
    }
    
    private void showLicences() {
        setChildScene(licences);
        licences.setOnMenuItemClickListener(this);
        currentSubmenu = SUBMENU_LICENCES;
    }
    
    private void hideLicences() {
        clearChildScene();
        showAbout();
        currentSubmenu = SUBMENU_ABOUT;
    }

    @Override
    public void updateUi() {
        mainMenu.updateUi();
    }

}
